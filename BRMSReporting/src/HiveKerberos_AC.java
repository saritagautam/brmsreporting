import org.apache.hadoop.security.UserGroupInformation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
public class HiveKerberos_AC {
	 private static final String CONF_KERBEROS_KEY = "hadoop.security.authentication";
	    private static final String CONF_KERBEROS_VALUE = "Kerberos";
	    private static final String HIVE_USER_PRINCIPAL = "hive/ip-172-42-50-39.lendingpoint.ad@LPANALYTICS.HDP";
	    private static final String HIVE_USER_KEYTAB = "C:\\Users\\sarita.gautam\\Desktop\\LendingPoint\\Hive\\conf_analytic\\hive.service.keytab";
	    private static final String HIVE_CONNECTION_STRING = "jdbc:hive2://52.204.138.8:10001/;principal=hive/ip-172-42-50-39.lendingpoint.ad@LPANALYTICS.HDP;transportMode=http;httpPath=cliservice;auth=kerberos;saslQop=auth-conf";
	    private static final String HIVE_JDBC_DRIVER = "org.apache.hive.jdbc.HiveDriver";
	    private static final String HIVE_QUERY = "select genesis__contact__c,referred_by__c,estimated_interest__c from salesforce.genesis__Applications__c limit 10";
	   // private static final String HIVE_QUERY_2 = "DESCRIBE salesforce.genesis__Applications__c";

	    public static void main(String args[]) {
	        try {
	            org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
	            conf.set(CONF_KERBEROS_KEY, CONF_KERBEROS_VALUE);
	            UserGroupInformation.setConfiguration(conf);
	            UserGroupInformation.loginUserFromKeytab(HIVE_USER_PRINCIPAL, HIVE_USER_KEYTAB);
	            Class.forName(HIVE_JDBC_DRIVER);
	            System.out.println("getting hiveConnection");
	            Connection hiveConnection = DriverManager.getConnection(HIVE_CONNECTION_STRING);
	            Statement statement = hiveConnection.createStatement();
	            ArrayList<String> columnNames = new ArrayList<String>();
	            try {
	                 
	                 ResultSet resultSet = statement.executeQuery(HIVE_QUERY);
	            	ResultSetMetaData metaData = resultSet.getMetaData();
	                while (resultSet.next()) {
	                	//System.out.println("metaData.getColumnName(1) : "+metaData.getColumnName(1));
	                	System.out.print(" : "+resultSet.getString("genesis__contact__c"));
	                	System.out.print(" : "+resultSet.getString("referred_by__c"));
	                	System.out.print(" : "+resultSet.getString("estimated_interest__c"));
	                	System.out.println("-----------");
	                	//columnNames.add(resultSet.getString(metaData.getColumnName(1)));
	                	//System.out.println(" Column names : "+resultSet.getString("scorecard_grade_overridden_by__c"));
	                    /*System.out.println(resultSet.getDate(4));
	                    System.out.println(resultSet.getBoolean(2));
	                    System.out.println(resultSet.getDouble(15));*/
	                }
	                
	                /*ResultSet resultSet2 = statement.executeQuery(HIVE_QUERY);
	                while (resultSet2.next()) {
	                	for(String column : columnNames)
	                		System.out.print(resultSet2.getString(column) +", " );
	                	System.out.println(" -- ");
	                }*/
	            } catch (Exception exception) {
	                exception.printStackTrace();
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
}
