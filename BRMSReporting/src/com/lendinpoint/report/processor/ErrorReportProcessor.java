package com.lendinpoint.report.processor;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.lendinpoint.common.DateCalculator;
import com.lendinpoint.model.ErrorReportData;
import com.lendinpoint.model.ErrorSummaryData;
import com.lendinpoint.model.ReconcilationData;
import com.lendinpoint.service.ServiceProvider;

public class ErrorReportProcessor {
	
	@Value("${excel.file.relative.path}")
	private String relativePath;
	
	@Value("${excel.error.file.name}")
	private String filePath;
	
	CellStyle headerStyle = null;
	
	CellStyle rowStyle = null;
	
	CellStyle columnHead = null;
	
	@Autowired
	DateCalculator dateCalculator;
	
	@Autowired
	ServiceProvider serviceProvider;
	
	SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
	
	Logger logger = LoggerFactory.getLogger(ErrorReportProcessor.class);

	@SuppressWarnings({ "unused", "deprecation" })
	public String processErrorReportData(
			List<ErrorReportData> dailyErrorReportData,
			List<ErrorSummaryData> dailySummaryData,int dailyTotalRequests,
			List<ErrorReportData> weeklyErrorReportData,
			List<ErrorSummaryData> weeklySummaryData,int weeklyTotalRequests,
			List<ErrorReportData> monthlyErrorReportData,
			List<ErrorSummaryData> monthlySummaryData,int monthlyTotalRequests) {
		
		String dailyErrorAcceptableRange = serviceProvider.getConfigByName("dailyErrorAcceptableRange");
		String weeklyErrorAcceptableRange = serviceProvider.getConfigByName("weeklyErrorAcceptableRange");
		String monthlyErrorAcceptableRange = serviceProvider.getConfigByName("monthlyErrorAcceptableRange");
		
		
		DateFormat formatterEST = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat formatterUTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				
		String fileName = null;
		HSSFSheet sheet=null,sheet1 = null,sheet2=null,sheet3=null,sheet4 = null,sheet5=null;
		try{
			HSSFWorkbook workbook = new HSSFWorkbook();
			sheet5 = workbook.createSheet("Daily Summary Report");
			sheet = workbook.createSheet("Daily Error Report");
			
			headerStyle = workbook.createCellStyle();//Create style
		    Font font = workbook.createFont();//Create font
		    font.setBold(true);
		    headerStyle.setFont(font);
		    headerStyle.setWrapText(true);
		    headerStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		    headerStyle.setAlignment(CellStyle.ALIGN_CENTER);	 
		    
		    headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		    headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		    
		    columnHead = workbook.createCellStyle();//Create style
		    columnHead.setFont(font);
		    columnHead.setWrapText(true);
		    columnHead.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		    
		    columnHead.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		    columnHead.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		    columnHead.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		    columnHead.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
		    columnHead.setAlignment(headerStyle.ALIGN_CENTER);	 
		    
		    rowStyle = workbook.createCellStyle();
		    
		    rowStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		    rowStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		    rowStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		    rowStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
		    rowStyle.setAlignment(HorizontalAlignment.LEFT);
		    
			if(weeklyErrorReportData.size()>0){
				sheet2 = workbook.createSheet("Weekly Summary Report");
				sheet1 = workbook.createSheet("Weekly Error Report");
			}
			if(monthlyErrorReportData.size()>0){
				sheet4 = workbook.createSheet("Monthly Summary Report");
				sheet3 = workbook.createSheet("Monthly Error Report");
			}
			int rowCounter = 0;
			 
		    short borderStyle = CellStyle.BORDER_MEDIUM;		    
			
			sheet= dailyErrorReport(dailyErrorReportData , sheet,workbook);
			sheet5 = dailySummaryReport(dailySummaryData,dailyErrorReportData.size(),dailyTotalRequests,sheet5,workbook,dailyErrorAcceptableRange);
			if(weeklyErrorReportData.size() > 0){
				sheet1= weeklyErrorReport(weeklyErrorReportData , sheet1,workbook);
				sheet2 = weeklySummaryReport(weeklySummaryData,weeklyErrorReportData.size(),weeklyTotalRequests,sheet2,workbook,weeklyErrorAcceptableRange);
			}
			if(monthlyErrorReportData.size() > 0){
				sheet3= monthlyErrorReport(monthlyErrorReportData , sheet3,workbook);
				sheet4 = monthlySummaryReport(monthlySummaryData,monthlyErrorReportData.size(),monthlyTotalRequests,sheet4,workbook,monthlyErrorAcceptableRange);
			}
			formatterEST = new SimpleDateFormat("yyyyMMdd_HHmm");
			formatterEST.setTimeZone(TimeZone.getTimeZone("EST"));
			String date = formatterEST.format(new Date());//
			fileName = this.relativePath+this.filePath+"_"+date+".xls";
			FileOutputStream fileOut = null;
			fileOut = new FileOutputStream(fileName);
			workbook.write(fileOut);
			fileOut.close();
		}catch (Exception e) {
			logger.error("Exception in processErrorReportData ");
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("Exception : "+sw.toString());
		}
		return fileName;
	}
	
	@SuppressWarnings({ "unused", "deprecation" })
	public HSSFSheet dailyErrorReport(List<ErrorReportData> dailyErrorReportData,HSSFSheet sheet,HSSFWorkbook workbook){
		
		ArrayList<String> datesListForQuery = dateCalculator.dateCalcuateForDailyReport(formatter);
		int rowCounter = 0;
		
		HSSFRow header1 = sheet.createRow(rowCounter);rowCounter++;
	    header1.createCell(0).setCellValue("Daily Error Report");
	   
	    HSSFRow header2 = sheet.createRow(rowCounter);rowCounter++;
	    header2.createCell(0).setCellValue("Report Period: " +datesListForQuery.get(0));
	    
	    HSSFRow header3 = sheet.createRow(rowCounter);rowCounter++;
	    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
	    
	    header1.getCell(0).setCellStyle(headerStyle);
	    header2.getCell(0).setCellStyle(headerStyle);
	    header3.getCell(0).setCellStyle(headerStyle);
	   
	    HSSFRow header4 = sheet.createRow(rowCounter);rowCounter++;
	   
	    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,6);
	    sheet.addMergedRegion(cellRange1);
	    
	    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,6);
	    sheet.addMergedRegion(cellRange2);
	    
	    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,6);
	    sheet.addMergedRegion(cellRange3);
	    
	    short borderStyle = CellStyle.BORDER_MEDIUM;
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
		
		HSSFRow rowhead = sheet.createRow(rowCounter);rowCounter++;
		rowhead.createCell(0).setCellValue("Sr. No.");
		rowhead.getCell(0).setCellStyle(columnHead);
		rowhead.createCell(1).setCellValue("Error");
		rowhead.getCell(1).setCellStyle(columnHead);
		rowhead.createCell(2).setCellValue("Error Date");
		rowhead.getCell(2).setCellStyle(columnHead);
		rowhead.createCell(3).setCellValue("Application ID");
		rowhead.getCell(3).setCellStyle(columnHead);
		rowhead.createCell(4).setCellValue("Customer ID");
		rowhead.getCell(4).setCellStyle(columnHead);
		/*rowhead.createCell(5).setCellValue("Loan ID");
		rowhead.getCell(5).setCellStyle(columnHead);*/
		rowhead.createCell(5).setCellValue("Session ID");
		rowhead.getCell(5).setCellStyle(columnHead);
		/*rowhead.createCell(7).setCellValue("Contract Status");
		rowhead.getCell(7).setCellStyle(columnHead);*/
		rowhead.createCell(6).setCellValue("Sales Rep/UW Assinged");
		rowhead.getCell(6).setCellStyle(columnHead);
		
		for(int i=0;i<=6;i++)
			if(i==5 )sheet.setColumnWidth(i, 255*40);
			else sheet.setColumnWidth(i, 255*25);
		
		int dataCounter = 1;
		for(ErrorReportData reportData :dailyErrorReportData){
			HSSFRow row = sheet.createRow(rowCounter);rowCounter++;
			row.createCell(0).setCellValue(dataCounter);dataCounter++;
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(reportData.getError());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(reportData.getErrorDate());
	        row.getCell(2).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(reportData.getApplicationId());
	        row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(4).setCellValue(reportData.getCustomerId());
	        row.getCell(4).setCellStyle(rowStyle);
	        row.createCell(5).setCellValue(reportData.getSessionId());
	        row.getCell(5).setCellStyle(rowStyle);
	        if(reportData.getFsAssigned()!= null)
	        	row.createCell(6).setCellValue(reportData.getFsAssigned());
	        else if(reportData.getCsAssigned()!= null)
	        	row.createCell(6).setCellValue(reportData.getCsAssigned());
	        else
	        	row.createCell(6).setCellValue("Not Assigned");
	        row.getCell(6).setCellStyle(rowStyle);
		}
		
		return sheet;
	}
	
	@SuppressWarnings("deprecation")
	public HSSFSheet weeklyErrorReport(List<ErrorReportData> errorReportData,HSSFSheet sheet ,HSSFWorkbook workbook){
		
		ArrayList<String> datesListForQuery = dateCalculator.dateCalcuateForWeeklyReport(formatter);
		
		int rowCounter = 0;
		
		HSSFRow header1 = sheet.createRow(rowCounter);rowCounter++;
	    header1.createCell(0).setCellValue("Weekly Error Report");
	   
	    HSSFRow header2 = sheet.createRow(rowCounter);rowCounter++;
	    header2.createCell(0).setCellValue("Report Period: " +datesListForQuery.get(0) + " to " + datesListForQuery.get(1));
	    
	    HSSFRow header3 = sheet.createRow(rowCounter);rowCounter++;
	    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
	    
	    header1.getCell(0).setCellStyle(headerStyle);
	    header2.getCell(0).setCellStyle(headerStyle);
	    header3.getCell(0).setCellStyle(headerStyle);
	   
	    @SuppressWarnings("unused")
		HSSFRow header4 = sheet.createRow(rowCounter);rowCounter++;
	   
	    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,6);
	    sheet.addMergedRegion(cellRange1);
	    
	    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,6);
	    sheet.addMergedRegion(cellRange2);
	    
	    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,6);
	    sheet.addMergedRegion(cellRange3);
	    
	    short borderStyle = CellStyle.BORDER_MEDIUM;
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
		
		HSSFRow rowhead = sheet.createRow(rowCounter);rowCounter++;
		rowhead.createCell(0).setCellValue("Sr. No.");
		rowhead.getCell(0).setCellStyle(columnHead);
		rowhead.createCell(1).setCellValue("Error");
		rowhead.getCell(1).setCellStyle(columnHead);
		rowhead.createCell(2).setCellValue("Error Date");
		rowhead.getCell(2).setCellStyle(columnHead);
		rowhead.createCell(3).setCellValue("Application ID");
		rowhead.getCell(3).setCellStyle(columnHead);
		rowhead.createCell(4).setCellValue("Customer ID");
		rowhead.getCell(4).setCellStyle(columnHead);
		/*rowhead.createCell(5).setCellValue("Loan ID");
		rowhead.getCell(5).setCellStyle(columnHead);*/
		rowhead.createCell(5).setCellValue("Session ID");
		rowhead.getCell(5).setCellStyle(columnHead);
		/*rowhead.createCell(7).setCellValue("Contract Status");
		rowhead.getCell(7).setCellStyle(columnHead);*/
		rowhead.createCell(6).setCellValue("Sales Rep/UW Assinged");
		rowhead.getCell(6).setCellStyle(columnHead);
		
		for(int i=0;i<=6;i++)
			if(i==5)sheet.setColumnWidth(i, 255*40);
			else sheet.setColumnWidth(i, 255*25);
		
		int dataCounter = 1;
		for(ErrorReportData reportData :errorReportData){
			HSSFRow row = sheet.createRow(rowCounter);rowCounter++;
			row.createCell(0).setCellValue(dataCounter);dataCounter++;
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(reportData.getError());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(reportData.getErrorDate());
	        row.getCell(2).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(reportData.getApplicationId());
	        row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(4).setCellValue(reportData.getCustomerId());
	        row.getCell(4).setCellStyle(rowStyle);
	        row.createCell(5).setCellValue(reportData.getSessionId());
	        row.getCell(5).setCellStyle(rowStyle);
	        if(reportData.getFsAssigned()!= null)
	        	row.createCell(6).setCellValue(reportData.getFsAssigned());
	        else if(reportData.getCsAssigned()!= null)
	        	row.createCell(6).setCellValue(reportData.getCsAssigned());
	        else
	        	row.createCell(6).setCellValue("Not Assigned");
	        row.getCell(6).setCellStyle(rowStyle);
		}
		
		return sheet;
	}
	
	@SuppressWarnings("deprecation")
	public HSSFSheet dailySummaryReport(
			List<ErrorSummaryData> dailySummaryData, int totalErrors,
			int dailyTotalRequests, HSSFSheet sheet, HSSFWorkbook workbook,String dailyErrorAcceptableRange) {
		
		ArrayList<String> datesListForQuery = dateCalculator.dateCalcuateForDailyReport(formatter);
		int rowCounter = 0;
		
		HSSFRow header1 = sheet.createRow(rowCounter);rowCounter++;
	    header1.createCell(0).setCellValue("Daily Summary Report");
	   
	    HSSFRow header2 = sheet.createRow(rowCounter);rowCounter++;
	    header2.createCell(0).setCellValue("Report Period: " +datesListForQuery.get(0));
	    
	    HSSFRow header3 = sheet.createRow(rowCounter);rowCounter++;
	    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
	    
	    header1.getCell(0).setCellStyle(headerStyle);
	    header2.getCell(0).setCellStyle(headerStyle);
	    header3.getCell(0).setCellStyle(headerStyle);
	   
	    @SuppressWarnings("unused")
		HSSFRow blankRow1 = sheet.createRow(rowCounter);rowCounter++;
	   
	    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,4);
	    sheet.addMergedRegion(cellRange1);
	    
	    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,4);
	    sheet.addMergedRegion(cellRange2);
	    
	    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,4);
	    sheet.addMergedRegion(cellRange3);
	    
	    short borderStyle = CellStyle.BORDER_MEDIUM;
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
		
	    HSSFRow rowheadSummary1 = sheet.createRow(rowCounter);rowCounter++;
	    rowheadSummary1.createCell(0).setCellValue("Total Error ");
	    rowheadSummary1.getCell(0).setCellStyle(rowStyle);
	    rowheadSummary1.createCell(1).setCellValue(totalErrors);
	    rowheadSummary1.getCell(1).setCellStyle(rowStyle);
	    
	    HSSFRow rowheadSummary2 = sheet.createRow(rowCounter);rowCounter++;
	    rowheadSummary2.createCell(0).setCellValue("Total Transactions ");
	    rowheadSummary2.getCell(0).setCellStyle(rowStyle);
	    rowheadSummary2.createCell(1).setCellValue(dailyTotalRequests);
	    rowheadSummary2.getCell(1).setCellStyle(rowStyle);
	    
	    HSSFRow rowheadSummary3 = sheet.createRow(rowCounter);rowCounter++;
	    rowheadSummary3.createCell(0).setCellValue("Acceptable Range ");
	    rowheadSummary3.getCell(0).setCellStyle(rowStyle);
	    rowheadSummary3.createCell(1).setCellValue(dailyErrorAcceptableRange);
	    rowheadSummary3.getCell(1).setCellStyle(rowStyle);
	    
	    @SuppressWarnings("unused")
		HSSFRow blankRow2 = sheet.createRow(rowCounter);rowCounter++;
	    
		HSSFRow rowhead = sheet.createRow(rowCounter);rowCounter++;
		rowhead.createCell(0).setCellValue("Sr. No.");
		rowhead.getCell(0).setCellStyle(columnHead);
		rowhead.createCell(1).setCellValue("Error Name");
		rowhead.getCell(1).setCellStyle(columnHead);
		rowhead.createCell(2).setCellValue("Frequency of error occurred");
		rowhead.getCell(2).setCellStyle(columnHead);
		rowhead.createCell(3).setCellValue("% of time error occurs vs Tot. errors");
		rowhead.getCell(3).setCellStyle(columnHead);
		rowhead.createCell(4).setCellValue("% of errors vs Tot. transactions");
		rowhead.getCell(4).setCellStyle(columnHead);
		/*rowhead.createCell(5).setCellValue("Acceptable range");
		rowhead.getCell(5).setCellStyle(headerStyle);*/
				
		for(int i=0;i<=4;i++)
			sheet.setColumnWidth(i, 255*25);
		
		int dataCounter = 1;
		Float percentageVsTotalErr = (float) 0;
		Float percentageVsTotalreq = (float) 0;
		DecimalFormat df = new DecimalFormat("#.00");
		for(ErrorSummaryData reportData :dailySummaryData){
			percentageVsTotalErr = ((float)((float)reportData.getCount()/(float)totalErrors)*100);
			percentageVsTotalreq = ((float)((float)reportData.getCount()/(float)dailyTotalRequests)*100);
			HSSFRow row = sheet.createRow(rowCounter);rowCounter++;
			row.createCell(0).setCellValue(dataCounter);dataCounter++;
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(reportData.getError());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(reportData.getCount());
	        row.getCell(2).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(df.format(percentageVsTotalErr));
	        row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(4).setCellValue(df.format(percentageVsTotalreq));
	        row.getCell(4).setCellStyle(rowStyle);
	       
		}
		
		return sheet;
	}
	
	
	@SuppressWarnings("deprecation")
	public HSSFSheet weeklySummaryReport(
			List<ErrorSummaryData> weeklySummaryData, int totalErrors,
			int weeklyTotalRequests, HSSFSheet sheet, HSSFWorkbook workbook,String weeklyErrorAcceptableRange) {
		
		ArrayList<String> datesListForQuery = dateCalculator.dateCalcuateForWeeklyReport(formatter);
		int rowCounter = 0;
		
		HSSFRow header1 = sheet.createRow(rowCounter);rowCounter++;
	    header1.createCell(0).setCellValue("Weekly Summary Report");
	   
	    HSSFRow header2 = sheet.createRow(rowCounter);rowCounter++;
	    header2.createCell(0).setCellValue("Report Period: " +datesListForQuery.get(0) + " to " + datesListForQuery.get(1));
	    
	    HSSFRow header3 = sheet.createRow(rowCounter);rowCounter++;
	    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
	    
	    header1.getCell(0).setCellStyle(headerStyle);
	    header2.getCell(0).setCellStyle(headerStyle);
	    header3.getCell(0).setCellStyle(headerStyle);
	   
	    @SuppressWarnings("unused")
		HSSFRow blankRow1 = sheet.createRow(rowCounter);rowCounter++;
	   
	    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,4);
	    sheet.addMergedRegion(cellRange1);
	    
	    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,4);
	    sheet.addMergedRegion(cellRange2);
	    
	    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,4);
	    sheet.addMergedRegion(cellRange3);
	    
	    short borderStyle = CellStyle.BORDER_MEDIUM;
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
		
	    HSSFRow rowheadSummary1 = sheet.createRow(rowCounter);rowCounter++;
	    rowheadSummary1.createCell(0).setCellValue("Total Error ");
	    rowheadSummary1.getCell(0).setCellStyle(rowStyle);
	    rowheadSummary1.createCell(1).setCellValue(totalErrors);
	    rowheadSummary1.getCell(1).setCellStyle(rowStyle);
	    
	    HSSFRow rowheadSummary2 = sheet.createRow(rowCounter);rowCounter++;
	    rowheadSummary2.createCell(0).setCellValue("Total Transactions ");
	    rowheadSummary2.getCell(0).setCellStyle(rowStyle);
	    rowheadSummary2.createCell(1).setCellValue(weeklyTotalRequests);
	    rowheadSummary2.getCell(1).setCellStyle(rowStyle);
	    
	    HSSFRow rowheadSummary3 = sheet.createRow(rowCounter);rowCounter++;
	    rowheadSummary3.createCell(0).setCellValue("Acceptable Range ");
	    rowheadSummary3.getCell(0).setCellStyle(rowStyle);
	    rowheadSummary3.createCell(1).setCellValue(weeklyErrorAcceptableRange);
	    rowheadSummary3.getCell(1).setCellStyle(rowStyle);
	    
	    @SuppressWarnings("unused")
		HSSFRow blankRow2 = sheet.createRow(rowCounter);rowCounter++;
	    
		HSSFRow rowhead = sheet.createRow(rowCounter);rowCounter++;
		rowhead.createCell(0).setCellValue("Sr. No.");
		rowhead.getCell(0).setCellStyle(columnHead);
		rowhead.createCell(1).setCellValue("Error Name");
		rowhead.getCell(1).setCellStyle(columnHead);
		rowhead.createCell(2).setCellValue("Frequency of error occurred");
		rowhead.getCell(2).setCellStyle(columnHead);
		rowhead.createCell(3).setCellValue("% of time error occurs vs Tot. errors");
		rowhead.getCell(3).setCellStyle(columnHead);
		rowhead.createCell(4).setCellValue("% of errors vs Tot. transactions");
		rowhead.getCell(4).setCellStyle(columnHead);
		/*rowhead.createCell(5).setCellValue("Acceptable range");
		rowhead.getCell(5).setCellStyle(headerStyle);*/
				
		for(int i=0;i<=4;i++)
			sheet.setColumnWidth(i, 255*25);
		
		int dataCounter = 1;
		Float percentageVsTotalErr = (float) 0;
		Float percentageVsTotalreq = (float) 0;
		DecimalFormat df = new DecimalFormat("#.00");
		for(ErrorSummaryData reportData :weeklySummaryData){
			percentageVsTotalErr = ((float)((float)reportData.getCount()/(float)totalErrors)*100);
			percentageVsTotalreq = ((float)((float)reportData.getCount()/(float)weeklyTotalRequests)*100);
			HSSFRow row = sheet.createRow(rowCounter);rowCounter++;
			row.createCell(0).setCellValue(dataCounter);dataCounter++;
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(reportData.getError());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(reportData.getCount());
	        row.getCell(2).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(df.format(percentageVsTotalErr));
	        row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(4).setCellValue(df.format(percentageVsTotalreq));
	        row.getCell(4).setCellStyle(rowStyle);
	       
		}
		
		return sheet;
	}

	@SuppressWarnings("deprecation")
	public HSSFSheet monthlyErrorReport(List<ErrorReportData> errorReportData,HSSFSheet sheet,HSSFWorkbook workbook){
	
		ArrayList<String> datesListForQuery = dateCalculator.dateCalcuateForMonthlyReport(formatter);
		
		int rowCounter = 0;
		
		HSSFRow header1 = sheet.createRow(rowCounter);rowCounter++;
	    header1.createCell(0).setCellValue("Monthly Error Report");
	   
	    HSSFRow header2 = sheet.createRow(rowCounter);rowCounter++;
	    header2.createCell(0).setCellValue("Report Period: " +datesListForQuery.get(0) + " to " + datesListForQuery.get(1));
	    
	    HSSFRow header3 = sheet.createRow(rowCounter);rowCounter++;
	    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
	    
	    header1.getCell(0).setCellStyle(headerStyle);
	    header2.getCell(0).setCellStyle(headerStyle);
	    header3.getCell(0).setCellStyle(headerStyle);
	   
	    @SuppressWarnings("unused")
		HSSFRow header4 = sheet.createRow(rowCounter);rowCounter++;
	   
	    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,6);
	    sheet.addMergedRegion(cellRange1);
	    
	    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,6);
	    sheet.addMergedRegion(cellRange2);
	    
	    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,6);
	    sheet.addMergedRegion(cellRange3);
	    
	    short borderStyle = CellStyle.BORDER_MEDIUM;
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
		
		HSSFRow rowhead = sheet.createRow(rowCounter);rowCounter++;
		rowhead.createCell(0).setCellValue("Sr. No.");
		rowhead.getCell(0).setCellStyle(columnHead);
		rowhead.createCell(1).setCellValue("Error");
		rowhead.getCell(1).setCellStyle(columnHead);
		rowhead.createCell(2).setCellValue("Error Date");
		rowhead.getCell(2).setCellStyle(columnHead);
		rowhead.createCell(3).setCellValue("Application ID");
		rowhead.getCell(3).setCellStyle(columnHead);
		rowhead.createCell(4).setCellValue("Customer ID");
		rowhead.getCell(4).setCellStyle(columnHead);
		rowhead.createCell(5).setCellValue("Session ID");
		rowhead.getCell(5).setCellStyle(columnHead);
		rowhead.createCell(6).setCellValue("Sales Rep/UW Assinged");
		rowhead.getCell(6).setCellStyle(columnHead);
		
		for(int i=0;i<=6;i++)
			if(i==5)sheet.setColumnWidth(i, 255*40);
			else sheet.setColumnWidth(i, 255*25);
		
		int dataCounter = 1;
		for(ErrorReportData reportData :errorReportData){
			HSSFRow row = sheet.createRow(rowCounter);rowCounter++;
			row.createCell(0).setCellValue(dataCounter);dataCounter++;
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(reportData.getError());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(reportData.getErrorDate());
	        row.getCell(2).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(reportData.getApplicationId());
	        row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(4).setCellValue(reportData.getCustomerId());
	        row.getCell(4).setCellStyle(rowStyle);
	        row.createCell(5).setCellValue(reportData.getSessionId());
	        row.getCell(5).setCellStyle(rowStyle);
	        if(reportData.getFsAssigned()!= null)
	        	row.createCell(6).setCellValue(reportData.getFsAssigned());
	        else if(reportData.getCsAssigned()!= null)
	        	row.createCell(6).setCellValue(reportData.getCsAssigned());
	        else
	        	row.createCell(6).setCellValue("Not Assigned");
	        row.getCell(6).setCellStyle(rowStyle);
		}
		
		return sheet;
}

	@SuppressWarnings("deprecation")
	public HSSFSheet monthlySummaryReport(
			List<ErrorSummaryData> weeklySummaryData, int totalErrors,
			int monthlyTotalRequests, HSSFSheet sheet, HSSFWorkbook workbook,
			String monthlyErrorAcceptableRange) {
	
		ArrayList<String> datesListForQuery = dateCalculator.dateCalcuateForMonthlyReport(formatter);
		
		int rowCounter = 0;
		
		HSSFRow header1 = sheet.createRow(rowCounter);rowCounter++;
	    header1.createCell(0).setCellValue("Monthly Summary Report");
	   
	    HSSFRow header2 = sheet.createRow(rowCounter);rowCounter++;
	    header2.createCell(0).setCellValue("Report Period: "+datesListForQuery.get(0) + " to " + datesListForQuery.get(1));
	    
	    HSSFRow header3 = sheet.createRow(rowCounter);rowCounter++;
	    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
	    
	    header1.getCell(0).setCellStyle(headerStyle);
	    header2.getCell(0).setCellStyle(headerStyle);
	    header3.getCell(0).setCellStyle(headerStyle);
	   
	    @SuppressWarnings("unused")
		HSSFRow blankRow1 = sheet.createRow(rowCounter);rowCounter++;
	    
	    HSSFRow rowheadSummary1 = sheet.createRow(rowCounter);rowCounter++;
	    rowheadSummary1.createCell(0).setCellValue("Total Error ");
	    rowheadSummary1.getCell(0).setCellStyle(rowStyle);
	    rowheadSummary1.createCell(1).setCellValue(totalErrors);
	    rowheadSummary1.getCell(1).setCellStyle(rowStyle);
	    
	    HSSFRow rowheadSummary2 = sheet.createRow(rowCounter);rowCounter++;
	    rowheadSummary2.createCell(0).setCellValue("Total Transactions ");
	    rowheadSummary2.getCell(0).setCellStyle(rowStyle);
	    rowheadSummary2.createCell(1).setCellValue(monthlyTotalRequests);
	    rowheadSummary2.getCell(1).setCellStyle(rowStyle);
	    
	    HSSFRow rowheadSummary3 = sheet.createRow(rowCounter);rowCounter++;
	    rowheadSummary3.createCell(0).setCellValue("Acceptable Range ");
	    rowheadSummary3.getCell(0).setCellStyle(rowStyle);
	    rowheadSummary3.createCell(1).setCellValue(monthlyErrorAcceptableRange);
	    rowheadSummary3.getCell(1).setCellStyle(rowStyle);
	   
	    @SuppressWarnings("unused")
		HSSFRow blankRow2 = sheet.createRow(rowCounter);rowCounter++;
	    
	    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,4);
	    sheet.addMergedRegion(cellRange1);
	    
	    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,4);
	    sheet.addMergedRegion(cellRange2);
	    
	    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,4);
	    sheet.addMergedRegion(cellRange3);
	    
	    short borderStyle = CellStyle.BORDER_MEDIUM;
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
		
		HSSFRow rowhead = sheet.createRow(rowCounter);rowCounter++;
		rowhead.createCell(0).setCellValue("Sr. No.");
		rowhead.getCell(0).setCellStyle(columnHead);
		rowhead.createCell(1).setCellValue("Error Name");
		rowhead.getCell(1).setCellStyle(columnHead);
		rowhead.createCell(2).setCellValue("Frequency of error occurred");
		rowhead.getCell(2).setCellStyle(columnHead);
		rowhead.createCell(3).setCellValue("% of time error occurs vs Tot. errors");
		rowhead.getCell(3).setCellStyle(columnHead);
		rowhead.createCell(4).setCellValue("% of errors vs Tot. transactions");
		rowhead.getCell(4).setCellStyle(columnHead);
		/*rowhead.createCell(5).setCellValue("Acceptable range");
		rowhead.getCell(5).setCellStyle(headerStyle);*/
				
		for(int i=0;i<=4;i++)
			sheet.setColumnWidth(i, 255*25);
		
		int dataCounter = 1;
		Float percentageVsTotalErr = (float) 0;
		Float percentageVsTotalreq = (float) 0;
		DecimalFormat df = new DecimalFormat("#.00");
			
		for(ErrorSummaryData reportData :weeklySummaryData){
			percentageVsTotalErr = ((float)((float)reportData.getCount()/(float)totalErrors)*100);
			percentageVsTotalreq = ((float)((float)reportData.getCount()/(float)monthlyTotalRequests)*100);
			HSSFRow row = sheet.createRow(rowCounter);rowCounter++;
			row.createCell(0).setCellValue(dataCounter);dataCounter++;
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(reportData.getError());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(reportData.getCount());
	        row.getCell(2).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(df.format(percentageVsTotalErr));
	        row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(4).setCellValue(df.format(percentageVsTotalreq));
	        row.getCell(4).setCellStyle(rowStyle);
		}
		return sheet;
	}
}
