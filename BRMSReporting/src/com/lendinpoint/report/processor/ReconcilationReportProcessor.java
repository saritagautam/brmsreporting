package com.lendinpoint.report.processor;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.sl.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.lendinpoint.common.DateCalculator;
import com.lendinpoint.model.ReconcilationData;

public class ReconcilationReportProcessor {
	
	@Value("${excel.file.relative.path}")
	private String relativePath;
	
	@Value("${excel.file.name}")
	private String filePath;
	
	CellStyle headerStyle = null;
	
	CellStyle rowStyle = null;
	
	@Autowired
	DateCalculator dateCalculator;
	
	SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
	
	Logger logger = LoggerFactory.getLogger(ReconcilationReportProcessor.class);

	@SuppressWarnings("deprecation")
	public String reportProcessor(List<ReconcilationData> totalGDSRequestData,
			List<ReconcilationData> gdsRequestFirstTimeData,
			List<ReconcilationData> gdsRequestMoreThanOnceData,
			List<ReconcilationData> gdsRequestsErroredOutData,
			List<ReconcilationData> gdsRequestsSentFirstTimeErroredOutData,
			List<ReconcilationData> gdsRequestSentMoreThanOnceErroredOutData) {

		logger.debug("Inside reportProcessor");
		DateFormat formatterEST = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat formatterUTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				
		String fileName = null;
		try {
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("Reconcilation summary");
			HSSFSheet sheet1 = workbook.createSheet("Total GDS requests");
			HSSFSheet sheet2 = workbook.createSheet("Total GDS requests sent first time in last month");
			HSSFSheet sheet3 = workbook.createSheet("Requests sent to GDS more than once in last month");
			HSSFSheet sheet4 = workbook.createSheet("Total number of requests which errored out");
			HSSFSheet sheet5 = workbook.createSheet("Requests erroring out first time in the last 30 days");
			HSSFSheet sheet6 = workbook.createSheet("Number of requests erroring out more than once in last 30 days");
			int summaryReportRowCounter=0;
			
			headerStyle = workbook.createCellStyle();//Create style
		    Font font = workbook.createFont();//Create font
		    font.setBold(true);
		    headerStyle.setFont(font);
		    headerStyle.setWrapText(true);
		    headerStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		    
		    
		    headerStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		    headerStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		    headerStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		    headerStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
		    headerStyle.setAlignment(headerStyle.ALIGN_CENTER);	 
		    
		    headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		    headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		    
		    rowStyle = workbook.createCellStyle();
		    
		    rowStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		    rowStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		    rowStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		    rowStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
		    rowStyle.setAlignment(HorizontalAlignment.LEFT);
		    
		    ArrayList<String> datesListForQuery = dateCalculator.dateCalcuateForMonthlyReport(formatter);
		    
		    HSSFRow header1 = sheet.createRow(summaryReportRowCounter);summaryReportRowCounter++;
		    header1.createCell(0).setCellValue("Monthly Reconciliation Report");
		   
		    HSSFRow header2 = sheet.createRow(summaryReportRowCounter);summaryReportRowCounter++;
		    header2.createCell(0).setCellValue("Report Period: " +datesListForQuery.get(0) + " to " + datesListForQuery.get(1));
		    
		    HSSFRow header3 = sheet.createRow(summaryReportRowCounter);summaryReportRowCounter++;
		    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
		   
		    HSSFRow header4 = sheet.createRow(summaryReportRowCounter);summaryReportRowCounter++;
		   
		    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,5);
		    sheet.addMergedRegion(cellRange1);
		    
		    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,5);
		    sheet.addMergedRegion(cellRange2);
		    
		    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,5);
		    sheet.addMergedRegion(cellRange3);
		    short borderStyle = CellStyle.BORDER_MEDIUM;
		    
		    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
		    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
		    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
		    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
		    
		    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
		    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
		    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
		    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
		    
		    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
		    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
		    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
		    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
		    
		    header1.getCell(0).setCellStyle(headerStyle);
		    header2.getCell(0).setCellStyle(headerStyle);
		    header3.getCell(0).setCellStyle(headerStyle);
		    
		    HSSFRow rowhead = sheet.createRow(summaryReportRowCounter);summaryReportRowCounter++;
			rowhead.createCell(0).setCellValue("Total number of GDS requests [Recon. Report 1]");
			rowhead.getCell(0).setCellStyle(headerStyle);
	        rowhead.createCell(1).setCellValue("Requests sent to GDS for the first time in last month[Recon. Report 2]");
	        rowhead.getCell(1).setCellStyle(headerStyle);
	        rowhead.createCell(2).setCellValue("Requests sent to GDS >1 times in last month[Recon. Report 3]");
	        rowhead.getCell(2).setCellStyle(headerStyle);
	        rowhead.createCell(3).setCellValue("Total number of requests which errored out in last month [Recon. Report 4]");
	        rowhead.getCell(3).setCellStyle(headerStyle);
	        rowhead.createCell(4).setCellValue("Requests erroring out first time in last month [Recon. Report 5]");
	        rowhead.getCell(4).setCellStyle(headerStyle);
	        rowhead.createCell(5).setCellValue("Number of requests erroring out >1 times in last month [Recon. Report 6]");
	        rowhead.getCell(5).setCellStyle(headerStyle);
	        
	        HSSFRow row = sheet.createRow(summaryReportRowCounter);
	        row.createCell(0).setCellValue(totalGDSRequestData.size());
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(gdsRequestFirstTimeData.size());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(gdsRequestMoreThanOnceData.size());
	        row.getCell(2).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(gdsRequestsErroredOutData.size());
	        row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(4).setCellValue(gdsRequestsSentFirstTimeErroredOutData.size());
	        row.getCell(4).setCellStyle(rowStyle);
	        row.createCell(5).setCellValue(gdsRequestSentMoreThanOnceErroredOutData.size());
	        row.getCell(5).setCellStyle(rowStyle);
	        
	        for(int i=0;i<6;i++)
	        	sheet.setColumnWidth(i, 255*25);
	        
	        //get total gds request data sheet
	        sheet1 = totalGDSRequestData(totalGDSRequestData,sheet1,workbook);
	        
	        //gds Requests Sent First Time
	        sheet2 = gdsRequestsSentFirstTime(gdsRequestFirstTimeData,sheet2,workbook);
	        
	        //gds Requests SentMore Than Once
	        sheet3 = gdsRequestsSentMoreThanOnce(gdsRequestMoreThanOnceData,sheet3,workbook);
	        
	        //Requests Errored Out
	        sheet4 = gdsRequestsErroredOut(gdsRequestsErroredOutData,sheet4,workbook);
	        
	        //Request sent first time errored out
	        sheet5 = gdsRequestsSentFirstTimeErroredOut(gdsRequestsSentFirstTimeErroredOutData,sheet5,workbook);
	        
	        //Request sent more than once errored out
	        sheet6 = gdsRequestsMoreThanOnceErroredOut(gdsRequestSentMoreThanOnceErroredOutData,sheet6,workbook);
	        
	        formatterEST = new SimpleDateFormat("yyyyMMdd_HHmm");
			formatterEST.setTimeZone(TimeZone.getTimeZone("EST"));
			String date = formatterEST.format(new Date());//
			fileName = this.relativePath+this.filePath+"_"+date+".xls";
			FileOutputStream fileOut = null;
			
			fileOut = new FileOutputStream(fileName);
			workbook.write(fileOut);
			fileOut.close();
		}catch (Exception e) {
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("Exception in Reconcilation report processor : "+sw.toString());
		}
		 
		return fileName;
	}
	
	public HSSFSheet totalGDSRequestData(List<ReconcilationData> gdsRequestData,HSSFSheet sheet, HSSFWorkbook workbook){
		int rowCounter=0;
				
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		cal.set(Calendar.DATE, 1);
		Date firstDateOfPreviousMonth = cal.getTime();

		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE)); // changed calendar to cal

		Date lastDateOfPreviousMonth = cal.getTime();
		
		HSSFRow header1 = sheet.createRow(rowCounter);rowCounter++;
	    header1.createCell(0).setCellValue("Total GDS requests");
	   
	    HSSFRow header2 = sheet.createRow(rowCounter);rowCounter++;
	    header2.createCell(0).setCellValue("Report Period: " +formatter.format(firstDateOfPreviousMonth) +" to "+formatter.format(lastDateOfPreviousMonth) );
	    
	    HSSFRow header3 = sheet.createRow(rowCounter);rowCounter++;
	    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
	   
	    HSSFRow header4 = sheet.createRow(rowCounter);rowCounter++;
	    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,4);
	    sheet.addMergedRegion(cellRange1);
	    
	    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,4);
	    sheet.addMergedRegion(cellRange2);
	    
	    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,4);
	    sheet.addMergedRegion(cellRange3);
	    header1.getCell(0).setCellStyle(headerStyle);
	    header2.getCell(0).setCellStyle(headerStyle);
	    header3.getCell(0).setCellStyle(headerStyle);
	    
	    short borderStyle = CellStyle.BORDER_MEDIUM;
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
	    
	    HSSFRow rowhead = sheet.createRow(rowCounter);rowCounter++;
		rowhead.createCell(0).setCellValue("Sr. No.");
		rowhead.getCell(0).setCellStyle(headerStyle);
		
		rowhead.createCell(1).setCellValue("Application ID");
		rowhead.getCell(1).setCellStyle(headerStyle);
		
		rowhead.createCell(2).setCellValue("Customer ID");
		rowhead.getCell(2).setCellStyle(headerStyle);
		
		rowhead.createCell(3).setCellValue("Session ID");
		rowhead.getCell(3).setCellStyle(headerStyle);
		
		rowhead.createCell(4).setCellValue("Loan ID");
		rowhead.getCell(4).setCellStyle(headerStyle);
		
		for(int i=0;i<5;i++)
			if(i==3)
				sheet.setColumnWidth(i, 255*45);
			else
				sheet.setColumnWidth(i, 255*20);
		
		int dataCounter = 1;
		for(ReconcilationData reportData :gdsRequestData){
			HSSFRow row = sheet.createRow(rowCounter);rowCounter++;
			row.createCell(0).setCellValue(dataCounter);dataCounter++;
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(reportData.getApplicationId());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(reportData.getCustomerId());
	        row.getCell(2).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(reportData.getSessionId());
	        row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(4).setCellValue(reportData.getLoanId());
	        row.getCell(4).setCellStyle(rowStyle);
		}
				
		return sheet;
	}
	
	public HSSFSheet gdsRequestsSentFirstTime(List<ReconcilationData> gdsRequestData,HSSFSheet sheet,HSSFWorkbook workbook){
		int rowCounter=0;
				
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		cal.set(Calendar.DATE, 1);
		Date firstDateOfPreviousMonth = cal.getTime();

		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE)); // changed calendar to cal

		Date lastDateOfPreviousMonth = cal.getTime();
		
		HSSFRow header1 = sheet.createRow(rowCounter);rowCounter++;
	    header1.createCell(0).setCellValue("Total GDS requests sent first time in last month");
	   
	    HSSFRow header2 = sheet.createRow(rowCounter);rowCounter++;
	    header2.createCell(0).setCellValue("Report Period: " +formatter.format(firstDateOfPreviousMonth) +" to "+formatter.format(lastDateOfPreviousMonth) );
	    
	    HSSFRow header3 = sheet.createRow(rowCounter);rowCounter++;
	    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
	   
	    HSSFRow header4 = sheet.createRow(rowCounter);rowCounter++;
	    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,3);
	    sheet.addMergedRegion(cellRange1);
	    
	    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,3);
	    sheet.addMergedRegion(cellRange2);
	    
	    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,3);
	    sheet.addMergedRegion(cellRange3);
	    header1.getCell(0).setCellStyle(headerStyle);
	    header2.getCell(0).setCellStyle(headerStyle);
	    header3.getCell(0).setCellStyle(headerStyle);
	    
	    short borderStyle = CellStyle.BORDER_MEDIUM;
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
	    
	    HSSFRow rowhead = sheet.createRow(rowCounter);rowCounter++;
		rowhead.createCell(0).setCellValue("Sr. No.");
		rowhead.getCell(0).setCellStyle(headerStyle);
		
		rowhead.createCell(1).setCellValue("Application ID");
		rowhead.getCell(1).setCellStyle(headerStyle);
		
		rowhead.createCell(2).setCellValue("Customer ID");
		rowhead.getCell(2).setCellStyle(headerStyle);
		
		//rowhead.createCell(3).setCellValue("Session ID");
		//rowhead.getCell(3).setCellStyle(headerStyle);
		
		rowhead.createCell(3).setCellValue("Loan ID");
		rowhead.getCell(3).setCellStyle(headerStyle);
		
		for(int i=0;i<4;i++)
			sheet.setColumnWidth(i, 255*20);
		
		int dataCounter = 1;
		for(ReconcilationData reportData :gdsRequestData){
			HSSFRow row = sheet.createRow(rowCounter);rowCounter++;
			row.createCell(0).setCellValue(dataCounter);dataCounter++;
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(reportData.getApplicationId());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(reportData.getCustomerId());
	        row.getCell(2).setCellStyle(rowStyle);
	        //row.createCell(3).setCellValue(reportData.getSessionId());
	       // row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(reportData.getLoanId());
	        row.getCell(3).setCellStyle(rowStyle);
		}
				
		return sheet;
	}
	
	public HSSFSheet gdsRequestsSentMoreThanOnce(List<ReconcilationData> gdsRequestData,HSSFSheet sheet, HSSFWorkbook workbook){
		int rowCounter=0;
				
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		cal.set(Calendar.DATE, 1);
		Date firstDateOfPreviousMonth = cal.getTime();

		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE)); // changed calendar to cal

		Date lastDateOfPreviousMonth = cal.getTime();
		
		HSSFRow header1 = sheet.createRow(rowCounter);rowCounter++;
	    header1.createCell(0).setCellValue("Requests sent to GDS more than once in last month");
	   
	    HSSFRow header2 = sheet.createRow(rowCounter);rowCounter++;
	    header2.createCell(0).setCellValue("Report Period: " +formatter.format(firstDateOfPreviousMonth) +" to "+formatter.format(lastDateOfPreviousMonth) );
	    
	    HSSFRow header3 = sheet.createRow(rowCounter);rowCounter++;
	    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
	   
	    HSSFRow header4 = sheet.createRow(rowCounter);rowCounter++;
	    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,4);
	    sheet.addMergedRegion(cellRange1);
	    
	    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,4);
	    sheet.addMergedRegion(cellRange2);
	    
	    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,4);
	    sheet.addMergedRegion(cellRange3);
	    header1.getCell(0).setCellStyle(headerStyle);
	    header2.getCell(0).setCellStyle(headerStyle);
	    header3.getCell(0).setCellStyle(headerStyle);
	    
	    short borderStyle = CellStyle.BORDER_MEDIUM;
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
	    
	    HSSFRow rowhead = sheet.createRow(rowCounter);rowCounter++;
		rowhead.createCell(0).setCellValue("Sr. No.");
		rowhead.getCell(0).setCellStyle(headerStyle);
		
		rowhead.createCell(1).setCellValue("Application ID");
		rowhead.getCell(1).setCellStyle(headerStyle);
		
		rowhead.createCell(2).setCellValue("Customer ID");
		rowhead.getCell(2).setCellStyle(headerStyle);
		
		rowhead.createCell(3).setCellValue("Loan ID");
		rowhead.getCell(3).setCellStyle(headerStyle);
		
		rowhead.createCell(4).setCellValue("No. of Cases");
		rowhead.getCell(4).setCellStyle(headerStyle);
		
		for(int i=0;i<5;i++)
			sheet.setColumnWidth(i, 255*20);
		
		int dataCounter = 1;
		for(ReconcilationData reportData :gdsRequestData){
			HSSFRow row = sheet.createRow(rowCounter);rowCounter++;
			row.createCell(0).setCellValue(dataCounter);dataCounter++;
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(reportData.getApplicationId());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(reportData.getCustomerId());
	        row.getCell(2).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(reportData.getLoanId());
	        row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(4).setCellValue(reportData.getAttemptsCount());
	        row.getCell(4).setCellStyle(rowStyle);
		}
				
		return sheet;
	}
	
	public HSSFSheet gdsRequestsErroredOut(List<ReconcilationData> gdsRequestData,HSSFSheet sheet, HSSFWorkbook workbook){
		int rowCounter=0;
				
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		cal.set(Calendar.DATE, 1);
		Date firstDateOfPreviousMonth = cal.getTime();

		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE)); // changed calendar to cal

		Date lastDateOfPreviousMonth = cal.getTime();
		
		HSSFRow header1 = sheet.createRow(rowCounter);rowCounter++;
	    header1.createCell(0).setCellValue("Total number of requests which errored out");
	   
	    HSSFRow header2 = sheet.createRow(rowCounter);rowCounter++;
	    header2.createCell(0).setCellValue("Report Period: " +formatter.format(firstDateOfPreviousMonth) +" to "+formatter.format(lastDateOfPreviousMonth) );
	    
	    HSSFRow header3 = sheet.createRow(rowCounter);rowCounter++;
	    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
	   
	    HSSFRow header4 = sheet.createRow(rowCounter);rowCounter++;
	   
	    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,4);
	    sheet.addMergedRegion(cellRange1);
	    
	    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,4);
	    sheet.addMergedRegion(cellRange2);
	    
	    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,4);
	    sheet.addMergedRegion(cellRange3);
	    
	    header1.getCell(0).setCellStyle(headerStyle);
	    header2.getCell(0).setCellStyle(headerStyle);
	    header3.getCell(0).setCellStyle(headerStyle);
	    
	    short borderStyle = CellStyle.BORDER_MEDIUM;
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
	    
	    HSSFRow rowhead = sheet.createRow(rowCounter);rowCounter++;
		rowhead.createCell(0).setCellValue("Sr. No.");
		rowhead.getCell(0).setCellStyle(headerStyle);
		
		rowhead.createCell(1).setCellValue("Application ID");
		rowhead.getCell(1).setCellStyle(headerStyle);
		
		rowhead.createCell(2).setCellValue("Customer ID");
		rowhead.getCell(2).setCellStyle(headerStyle);
		
		rowhead.createCell(3).setCellValue("Session ID");
		rowhead.getCell(3).setCellStyle(headerStyle);
		
		rowhead.createCell(4).setCellValue("Loan ID");
		rowhead.getCell(4).setCellStyle(headerStyle);
		
		for(int i=0;i<5;i++)
			if(i==3)
				sheet.setColumnWidth(i, 255*45);
			else
				sheet.setColumnWidth(i, 255*20);
		
		int dataCounter = 1;
		for(ReconcilationData reportData :gdsRequestData){
			HSSFRow row = sheet.createRow(rowCounter);rowCounter++;
			row.createCell(0).setCellValue(dataCounter);dataCounter++;
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(reportData.getApplicationId());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(reportData.getCustomerId());
	        row.getCell(2).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(reportData.getSessionId());
	        row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(4).setCellValue(reportData.getLoanId());
	        row.getCell(4).setCellStyle(rowStyle);
		}
				
		return sheet;
	}
	
	public HSSFSheet gdsRequestsMoreThanOnceErroredOut(List<ReconcilationData> gdsRequestData,HSSFSheet sheet,HSSFWorkbook workbook){
		int rowCounter=0;
				
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		cal.set(Calendar.DATE, 1);
		Date firstDateOfPreviousMonth = cal.getTime();

		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE)); // changed calendar to cal

		Date lastDateOfPreviousMonth = cal.getTime();
		
		HSSFRow header1 = sheet.createRow(rowCounter);rowCounter++;
	    header1.createCell(0).setCellValue("Number of requests erroring out more than once in last month");
	   
	    HSSFRow header2 = sheet.createRow(rowCounter);rowCounter++;
	    header2.createCell(0).setCellValue("Report Period: " +formatter.format(firstDateOfPreviousMonth) +" to "+formatter.format(lastDateOfPreviousMonth) );
	    
	    HSSFRow header3 = sheet.createRow(rowCounter);rowCounter++;
	    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
	   
	    HSSFRow header4 = sheet.createRow(rowCounter);rowCounter++;
	    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,4);
	    sheet.addMergedRegion(cellRange1);
	    
	    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,4);
	    sheet.addMergedRegion(cellRange2);
	    
	    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,4);
	    sheet.addMergedRegion(cellRange3);
	    
	    header1.getCell(0).setCellStyle(headerStyle);
	    header2.getCell(0).setCellStyle(headerStyle);
	    header3.getCell(0).setCellStyle(headerStyle);
	    
	    short borderStyle = CellStyle.BORDER_MEDIUM;
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
	    
	    HSSFRow rowhead = sheet.createRow(rowCounter);rowCounter++;
		rowhead.createCell(0).setCellValue("Sr. No.");
		rowhead.getCell(0).setCellStyle(headerStyle);
		
		rowhead.createCell(1).setCellValue("Application ID");
		rowhead.getCell(1).setCellStyle(headerStyle);
		
		rowhead.createCell(2).setCellValue("Customer ID");
		rowhead.getCell(2).setCellStyle(headerStyle);
		
		//rowhead.createCell(3).setCellValue("Session ID");
		//rowhead.getCell(3).setCellStyle(headerStyle);
		
		rowhead.createCell(3).setCellValue("Loan ID");
		rowhead.getCell(3).setCellStyle(headerStyle);
		
		rowhead.createCell(4).setCellValue("No. of Cases");
		rowhead.getCell(4).setCellStyle(headerStyle);
		
		for(int i=0;i<=4;i++)
			sheet.setColumnWidth(i, 255*20);
		
		int dataCounter = 1;
		for(ReconcilationData reportData :gdsRequestData){
			HSSFRow row = sheet.createRow(rowCounter);rowCounter++;
			row.createCell(0).setCellValue(dataCounter);dataCounter++;
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(reportData.getApplicationId());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(reportData.getCustomerId());
	        row.getCell(2).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(reportData.getLoanId());
	        row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(4).setCellValue(reportData.getAttemptsCount());
	        row.getCell(4).setCellStyle(rowStyle);
		}
				
		return sheet;
	}
	
	public HSSFSheet gdsRequestsSentFirstTimeErroredOut(List<ReconcilationData> gdsRequestData,HSSFSheet sheet,HSSFWorkbook workbook){
		int rowCounter=0;
				
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		cal.set(Calendar.DATE, 1);
		Date firstDateOfPreviousMonth = cal.getTime();

		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE)); // changed calendar to cal

		Date lastDateOfPreviousMonth = cal.getTime();
		
		HSSFRow header1 = sheet.createRow(rowCounter);rowCounter++;
	    header1.createCell(0).setCellValue("Requests erroring out first time in last month");
	   
	    HSSFRow header2 = sheet.createRow(rowCounter);rowCounter++;
	    header2.createCell(0).setCellValue("Report Period: " +formatter.format(firstDateOfPreviousMonth) +" to "+formatter.format(lastDateOfPreviousMonth) );
	    
	    HSSFRow header3 = sheet.createRow(rowCounter);rowCounter++;
	    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
	   
	    HSSFRow header4 = sheet.createRow(rowCounter);rowCounter++;
	    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,4);
	    sheet.addMergedRegion(cellRange1);
	    
	    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,4);
	    sheet.addMergedRegion(cellRange2);
	    
	    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,4);
	    sheet.addMergedRegion(cellRange3);
	    
	    header1.getCell(0).setCellStyle(headerStyle);
	    header2.getCell(0).setCellStyle(headerStyle);
	    header3.getCell(0).setCellStyle(headerStyle);
	    
	    short borderStyle = CellStyle.BORDER_MEDIUM;
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
	    
	    HSSFRow rowhead = sheet.createRow(rowCounter);rowCounter++;
		rowhead.createCell(0).setCellValue("Sr. No.");
		rowhead.getCell(0).setCellStyle(headerStyle);
		
		rowhead.createCell(1).setCellValue("Application ID");
		rowhead.getCell(1).setCellStyle(headerStyle);
		
		rowhead.createCell(2).setCellValue("Customer ID");
		rowhead.getCell(2).setCellStyle(headerStyle);
		
		rowhead.createCell(3).setCellValue("Session ID");
		rowhead.getCell(3).setCellStyle(headerStyle);
		
		rowhead.createCell(4).setCellValue("Loan ID");
		rowhead.getCell(4).setCellStyle(headerStyle);
		
		for(int i=0;i<5;i++)
			if(i==3)
				sheet.setColumnWidth(i, 255*45);
			else
				sheet.setColumnWidth(i, 255*20);
		
		int dataCounter = 1;
		for(ReconcilationData reportData :gdsRequestData){
			HSSFRow row = sheet.createRow(rowCounter);rowCounter++;
			row.createCell(0).setCellValue(dataCounter);dataCounter++;
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(reportData.getApplicationId());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(reportData.getCustomerId());
	        row.getCell(2).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(reportData.getSessionId());
	        row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(4).setCellValue(reportData.getLoanId());
	        row.getCell(4).setCellStyle(rowStyle);
		}
				
		return sheet;
	}
}
