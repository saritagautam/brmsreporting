package com.lendinpoint.report.processor;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.lendinpoint.common.DateCalculator;
import com.lendinpoint.model.DurationReportData;
import com.lendinpoint.model.RulesViolatedData;

public class DurationReportProcessor {
	@Value("${excel.file.relative.path}")
	private String relativePath;

	@Value("${excel.duration.file.name}")
	private String filePath;

	CellStyle headerStyle = null;

	CellStyle rowStyle = null;

	CellStyle columnHead = null;

	SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
	
	Logger logger = LoggerFactory.getLogger(DurationReportProcessor.class);
	
	@Autowired
	DateCalculator dateCalculator;

	public String processDurationReportData(
			List<DurationReportData> dailyDurationReportData,
			List<DurationReportData> weeklyDurationReportData,
			List<DurationReportData> monthlyDurationReportData) {
		DateFormat formatterEST = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat formatterUTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String fileName = null;
		HSSFSheet sheet = null, sheet1 = null, sheet2 = null;

		try {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -1);
			cal.set(Calendar.DATE, 1);
			int monthDays = cal.getActualMaximum(Calendar.DATE);
			HSSFWorkbook workbook = new HSSFWorkbook();
			sheet = workbook.createSheet("Daily Duration Report");

			headerStyle = workbook.createCellStyle();// Create style
			Font font = workbook.createFont();// Create font
			font.setBold(true);
			headerStyle.setFont(font);
			headerStyle.setWrapText(true);
			headerStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
			headerStyle.setAlignment(headerStyle.ALIGN_CENTER);
			headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT
					.getIndex());
			headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

			columnHead = workbook.createCellStyle();// Create style
			columnHead.setFont(font);
			columnHead.setWrapText(true);
			columnHead.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

			columnHead.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
			columnHead.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
			columnHead.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
			columnHead.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
			columnHead.setAlignment(headerStyle.ALIGN_CENTER);

			rowStyle = workbook.createCellStyle();

			rowStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
			rowStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
			rowStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
			rowStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
			rowStyle.setAlignment(HorizontalAlignment.LEFT);

			if (weeklyDurationReportData.size() > 0) {
				sheet1 = workbook.createSheet("Weekly Duration Report");
			}
			if (monthlyDurationReportData.size() > 0) {
				sheet2 = workbook.createSheet("Monthly Duration Report");
			}
			int rowCounter = 0;

			short borderStyle = CellStyle.BORDER_MEDIUM;
			sheet = durationReport(dailyDurationReportData, sheet, workbook, 1);

			if (weeklyDurationReportData.size() > 0) {
				sheet1 = durationReport(weeklyDurationReportData, sheet1,workbook,7);
			}
			if (monthlyDurationReportData.size() > 0) {
				sheet2 = durationReport(monthlyDurationReportData,sheet2, workbook,monthDays);
			}
			formatterEST = new SimpleDateFormat("yyyyMMdd_HHmm");
			formatterEST.setTimeZone(TimeZone.getTimeZone("EST"));
			String date = formatterEST.format(new Date());//
			fileName = this.relativePath + this.filePath + "_" + date + ".xls";
			FileOutputStream fileOut = null;
			logger.debug("filename ::::: " + fileName);
			fileOut = new FileOutputStream(fileName);
			workbook.write(fileOut);
			fileOut.close();
		} catch (Exception e) {
			logger.error("Exception in duration report processor");
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("Exception : "+sw.toString());
		} 

		return fileName;
	}

	public HSSFSheet durationReport(List<DurationReportData> dailyReportData,
			HSSFSheet sheet, HSSFWorkbook workbook, int days) {

		int[] durationArray = new int[dailyReportData.size()];

		List<Integer> percentiles = new ArrayList<Integer>(Arrays.asList(50,
				75, 90, 95, 99));
		// 100th percentile is not possible
		List<Integer> durationList = new ArrayList<Integer>();
		int k = 0;
		int totalDuration = 0;
		for (DurationReportData data : dailyReportData) {
			durationArray[k] = data.getElapsedTime();
			k++;
			totalDuration += data.getElapsedTime();
		}

		int rowCounter = 0;

		HSSFRow header1 = sheet.createRow( rowCounter);
		rowCounter++;
		

		HSSFRow header2 = sheet.createRow( rowCounter);
		rowCounter++;
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		ArrayList<String> datesListForQuery = new ArrayList<String>();
		if (days == 1){
			datesListForQuery = dateCalculator.dateCalcuateForDailyReport(formatter);
			header1.createCell(0).setCellValue("Daily Duration Report");
			
			header2.createCell(0).setCellValue(
					"Report Period: "  +datesListForQuery.get(0));
		}
		else if (days == 7) {
			datesListForQuery = dateCalculator.dateCalcuateForWeeklyReport(formatter);
			header1.createCell(0).setCellValue("Weekly Duration Report");
			header2.createCell(0).setCellValue(
					"Report Period: " +datesListForQuery.get(0) + " to " + datesListForQuery.get(1));
		} else if (days > 7) {
			header1.createCell(0).setCellValue("Monthly Duration Report");
			datesListForQuery = dateCalculator.dateCalcuateForMonthlyReport(formatter);
			header2.createCell(0).setCellValue(
					"Report Period: "+datesListForQuery.get(0) + " to " + datesListForQuery.get(1));
		}

		HSSFRow header3 = sheet.createRow( rowCounter);
		rowCounter++;
		header3.createCell(0).setCellValue("Creation Date: " + formatter.format(new Date()));

		header1.getCell(0).setCellStyle(headerStyle);
		header2.getCell(0).setCellStyle(headerStyle);
		header3.getCell(0).setCellStyle(headerStyle);

		HSSFRow header4 = sheet.createRow( rowCounter);
		rowCounter++;

		CellRangeAddress cellRange1 = new CellRangeAddress(0, 0, 0, 5);
		sheet.addMergedRegion(cellRange1);

		CellRangeAddress cellRange2 = new CellRangeAddress(1, 1, 0, 5);
		sheet.addMergedRegion(cellRange2);

		CellRangeAddress cellRange3 = new CellRangeAddress(2, 2, 0, 5);
		sheet.addMergedRegion(cellRange3);

		short borderStyle = CellStyle.BORDER_MEDIUM;
		RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
		RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
		RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
		RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);

		RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
		RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
		RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
		RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);

		RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
		RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
		RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
		RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);

		HSSFRow summary1 = sheet.createRow( rowCounter);
		rowCounter++;
		summary1.createCell(0).setCellValue("Total Hits");
		summary1.getCell(0).setCellStyle(columnHead);
		summary1.createCell(1).setCellValue(dailyReportData.size());// total hit
																	// count
		summary1.getCell(1).setCellStyle(columnHead);

		HSSFRow summary2 = sheet.createRow( rowCounter);
		rowCounter++;
		summary2.createCell(0)
				.setCellValue("Total Time Taken (in miliseconds)");
		summary2.getCell(0).setCellStyle(columnHead);
		summary2.createCell(1).setCellValue(totalDuration);// total time
		summary2.getCell(1).setCellStyle(columnHead);

		if(dailyReportData.size()>0){
			HSSFRow summary3 = sheet.createRow( rowCounter);
			rowCounter++;
			summary3.createCell(0).setCellValue("Avg. Time (in miliseconds)");
			summary3.getCell(0).setCellStyle(columnHead);
			summary3.createCell(1).setCellValue(
					totalDuration / dailyReportData.size());// average time
			summary3.getCell(1).setCellStyle(columnHead);
	
			int index = 0;
			for (Integer percentile : percentiles) {
				HSSFRow percentileRow = sheet.createRow( rowCounter);
				rowCounter++;
				percentileRow.createCell(0).setCellValue(percentile + "%");
				percentileRow.getCell(0).setCellStyle(columnHead);
				// calculate percentile
				float percentileValue = 0;
				float percentIndex=0;
				if ((percentile * durationArray.length) % 100 == 0) {
					index = (percentile * durationArray.length) / 100;
					index=index-1;
					percentileValue = (durationArray[index] + durationArray[index + 1]) / 2;
				} else {
					percentIndex = (float)((float)percentile * durationArray.length)/(float)100;
					//int rounded = (int) (unrounded + 0.5);
					index = Math.round(percentIndex);
					index=index-1;
					//System.out.println("percentIndex : "+percentIndex +", index : "+index);
					percentileValue = durationArray[index];
				}
				percentileRow.createCell(1).setCellValue(percentileValue);// percentile
				percentileRow.getCell(1).setCellStyle(columnHead);
			}
		}
			HSSFRow blankrow1 = sheet.createRow( rowCounter);
			rowCounter++;
			HSSFRow blankrow2 = sheet.createRow( rowCounter);
			rowCounter++;
	
			HSSFRow rowhead = sheet.createRow( rowCounter);
			rowCounter++;
			rowhead.createCell(0).setCellValue("Sr. No.");
			rowhead.getCell(0).setCellStyle(columnHead);
			rowhead.createCell(1).setCellValue("Application ID");
			rowhead.getCell(1).setCellStyle(columnHead);
			rowhead.createCell(2).setCellValue("Customer ID");
			rowhead.getCell(2).setCellStyle(columnHead);
			rowhead.createCell(3).setCellValue("Loan ID");
			rowhead.getCell(3).setCellStyle(columnHead);
			rowhead.createCell(4).setCellValue("Session ID");
			rowhead.getCell(4).setCellStyle(columnHead);
			rowhead.createCell(5).setCellValue("Elapsed Time (ms)");
			rowhead.getCell(5).setCellStyle(columnHead);
	
			for (int i = 0; i < 6; i++)
				if (i == 4)
					sheet.setColumnWidth(i, 255 * 40);
				else
					sheet.setColumnWidth(i, 255 * 25);
	
			int dataCounter = 1;
			for (DurationReportData reportData : dailyReportData) {
				HSSFRow row = sheet.createRow( rowCounter);
				rowCounter++;
				row.createCell(0).setCellValue(dataCounter);
				dataCounter++;
				row.getCell(0).setCellStyle(rowStyle);
				row.createCell(1).setCellValue(reportData.getApplicationId());
				row.getCell(1).setCellStyle(rowStyle);
				row.createCell(2).setCellValue(reportData.getCustomerId());
				row.getCell(2).setCellStyle(rowStyle);
				row.createCell(3).setCellValue(reportData.getLoanId());
				row.getCell(3).setCellStyle(rowStyle);
				row.createCell(4).setCellValue(reportData.getSessionId());
				row.getCell(4).setCellStyle(rowStyle);
				row.createCell(5).setCellValue(reportData.getElapsedTime());// set
																			// elapsed
																			// time
																			// here
				row.getCell(5).setCellStyle(rowStyle);
			}
		//if
		return sheet;
	}

	
}
