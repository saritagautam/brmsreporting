package com.lendinpoint.report.processor;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.lendinpoint.common.DateCalculator;
import com.lendinpoint.model.ErrorReportData;
import com.lendinpoint.model.RulesViolatedData;
import com.lendinpoint.model.RulesViolatedSummaryData;

public class RulesViolatedReportProcessor {
	
	@Value("${excel.file.relative.path}")
	private String relativePath;
	
	@Value("${excel.rules.file.name}")
	private String filePath;
	
	@Autowired
	DateCalculator dateCalculator;
	
	CellStyle headerStyle = null;
	
	CellStyle rowStyle = null;
	
	CellStyle columnHead = null;
	
	SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
	
	public String processRulesViolatedReportData(List<RulesViolatedData> dailyRulesViolatedData,
			LinkedHashMap<String,RulesViolatedSummaryData> dailyRulesViolatedSummaryData,
			List<RulesViolatedData> weeklyRulesViolatedData,
			LinkedHashMap<String,RulesViolatedSummaryData> weeklyRulesViolatedSummaryData,
			List<RulesViolatedData> monthlyRulesViolatedData,
			LinkedHashMap<String,RulesViolatedSummaryData> monthlyRulesViolatedSummaryData){
		
		DateFormat formatterEST = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat formatterUTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		String fileName = null;
		HSSFSheet sheet=null,sheet1 = null,sheet2=null,sheet3=null,sheet4 = null;
		
		try{
			HSSFWorkbook workbook = new HSSFWorkbook();
			sheet = workbook.createSheet("Daily Rules Violated Summary Report");
			
			headerStyle = workbook.createCellStyle();//Create style
		    Font font = workbook.createFont();//Create font
		    font.setBold(true);
		    headerStyle.setFont(font);
		    headerStyle.setWrapText(true);
		    headerStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		    headerStyle.setAlignment(headerStyle.ALIGN_CENTER);
		    headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		    headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		    
		    columnHead = workbook.createCellStyle();//Create style
		    columnHead.setFont(font);
		    columnHead.setWrapText(true);
		    columnHead.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		    
		    columnHead.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		    columnHead.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		    columnHead.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		    columnHead.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
		    columnHead.setAlignment(headerStyle.ALIGN_CENTER);	 
		    
		    rowStyle = workbook.createCellStyle();
		    
		    rowStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
		    rowStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
		    rowStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
		    rowStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
		    rowStyle.setAlignment(HorizontalAlignment.LEFT);
		    
			if(weeklyRulesViolatedData.size()>0){
				//sheet1 = workbook.createSheet("Weekly Rules Violated Report");
				sheet2 = workbook.createSheet("Weekly Rules Violated Summary Report");
			}
			if(monthlyRulesViolatedData.size()>0){
				//sheet3 = workbook.createSheet("Monthly Rules Violated Report");
				sheet4 = workbook.createSheet("Monthly Rules Violated Summary Report");
			}
			int rowCounter = 0;
			 
		    short borderStyle = CellStyle.BORDER_MEDIUM;		    
			
			sheet= dailySummaryReport(dailyRulesViolatedSummaryData , sheet,workbook);
			
			if(weeklyRulesViolatedData.size() > 0){
				//sheet1= weeklyRulesViolatedReport(weeklyRulesViolatedData , sheet1,workbook);
				sheet2 = weeklySummaryReport(weeklyRulesViolatedSummaryData,sheet2,workbook);
			}
			if(monthlyRulesViolatedData.size() > 0){
				//sheet3= monthlyRulesViolatedReport(monthlyRulesViolatedData , sheet3,workbook);
				sheet4 = monthlySummaryReport(monthlyRulesViolatedSummaryData,sheet4,workbook);
			}
			formatterEST = new SimpleDateFormat("yyyyMMdd_HHmm");
			formatterEST.setTimeZone(TimeZone.getTimeZone("EST"));
			String date = formatterEST.format(new Date());//
			fileName = this.relativePath+this.filePath+"_"+date+".xls";
			FileOutputStream fileOut = null;
			System.out.println("filenmae ::::: " +fileName);
			fileOut = new FileOutputStream(fileName);
			workbook.write(fileOut);
			fileOut.close();
		}catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return fileName;
	}
	
public HSSFSheet dailySummaryReport(Map<String,RulesViolatedSummaryData> weeklyRulesViolatedSummaryData,HSSFSheet sheet,HSSFWorkbook workbook){
		
		int rowCounter = 0;
		
		ArrayList<String> datesListForQuery = dateCalculator.dateCalcuateForDailyReport(formatter);
		
		HSSFRow header1 = sheet.createRow(rowCounter);rowCounter++;
	    header1.createCell(0).setCellValue("Daily Rules Violated Summary Report");
	   
	    HSSFRow header2 = sheet.createRow(rowCounter);rowCounter++;
	    header2.createCell(0).setCellValue("Report Period: " +datesListForQuery.get(0));
	    
	    HSSFRow header3 = sheet.createRow(rowCounter);rowCounter++;
	    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
	    
	    header1.getCell(0).setCellStyle(headerStyle);
	    header2.getCell(0).setCellStyle(headerStyle);
	    header3.getCell(0).setCellStyle(headerStyle);
	   
	    HSSFRow header4 = sheet.createRow(rowCounter);rowCounter++;
	   
	    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,6);
	    sheet.addMergedRegion(cellRange1);
	    
	    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,6);
	    sheet.addMergedRegion(cellRange2);
	    
	    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,6);
	    sheet.addMergedRegion(cellRange3);
	    
	    short borderStyle = CellStyle.BORDER_MEDIUM;
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
		
		HSSFRow rowhead = sheet.createRow(rowCounter);rowCounter++;
		rowhead.createCell(0).setCellValue("Sr. No.");
		rowhead.getCell(0).setCellStyle(columnHead);
		rowhead.createCell(1).setCellValue("Rule Name");
		rowhead.getCell(1).setCellStyle(columnHead);
		rowhead.createCell(2).setCellValue("# of Processed records");
		rowhead.getCell(2).setCellStyle(columnHead);
		rowhead.createCell(3).setCellValue("No. Approved");
		rowhead.getCell(3).setCellStyle(columnHead);
		rowhead.createCell(4).setCellValue("No. Declined");
		rowhead.getCell(4).setCellStyle(columnHead);
		rowhead.createCell(5).setCellValue("% Approved");
		rowhead.getCell(5).setCellStyle(columnHead);
		rowhead.createCell(6).setCellValue("% Declined");
		rowhead.getCell(6).setCellStyle(columnHead);
		
		for(int i=0;i<=6;i++)
			if(i==1)sheet.setColumnWidth(i, 255*40);
			else
				sheet.setColumnWidth(i, 255*25);
		
		int dataCounter = 1;
		DecimalFormat df = new DecimalFormat("#.00");
		Float passPercent = (float) 0;
		Float failPercent = (float) 0;
		Iterator<RulesViolatedSummaryData> it = weeklyRulesViolatedSummaryData.values().iterator();
		//for (String key: weeklyRulesViolatedSummaryData.keySet()) {
		while(it.hasNext()){
			RulesViolatedSummaryData reportData = it.next();
			Float total = (float) (reportData.getRuleFailCount()+reportData.getRulePassCount());
			passPercent = ((float)((float)reportData.getRulePassCount()/total))*100;
			failPercent = ((float)((float)reportData.getRuleFailCount()/total))*100;
			HSSFRow row = sheet.createRow(rowCounter);rowCounter++;
			row.createCell(0).setCellValue(dataCounter);dataCounter++;
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(reportData.getRuleName());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(reportData.getRuleFailCount()+reportData.getRulePassCount());
	        row.getCell(2).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(reportData.getRulePassCount());
	        row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(4).setCellValue(reportData.getRuleFailCount());
	        row.getCell(4).setCellStyle(rowStyle);
	        row.createCell(5).setCellValue(df.format(passPercent));
	        row.getCell(5).setCellStyle(rowStyle);
	        row.createCell(6).setCellValue(df.format(failPercent));
	        row.getCell(6).setCellStyle(rowStyle);
		}
		
		return sheet;
	}
	
	public HSSFSheet dailyRulesViolatedReport(List<RulesViolatedData> dailyRulesViolatedData ,HSSFSheet sheet, HSSFWorkbook workbook){
		
		int rowCounter = 0;
		
		ArrayList<String> datesListForQuery = dateCalculator.dateCalcuateForDailyReport(formatter);
		
		HSSFRow header1 = sheet.createRow(rowCounter);rowCounter++;
	    header1.createCell(0).setCellValue("Daily Rules Violated Report");
	   
	    HSSFRow header2 = sheet.createRow(rowCounter);rowCounter++;
	    header2.createCell(0).setCellValue("Report Period: " +datesListForQuery.get(0));
	    
	    HSSFRow header3 = sheet.createRow(rowCounter);rowCounter++;
	    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
	    
	    header1.getCell(0).setCellStyle(headerStyle);
	    header2.getCell(0).setCellStyle(headerStyle);
	    header3.getCell(0).setCellStyle(headerStyle);
	   
	    HSSFRow header4 = sheet.createRow(rowCounter);rowCounter++;
	   
	    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,5);
	    sheet.addMergedRegion(cellRange1);
	    
	    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,5);
	    sheet.addMergedRegion(cellRange2);
	    
	    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,5);
	    sheet.addMergedRegion(cellRange3);
	    
	    short borderStyle = CellStyle.BORDER_MEDIUM;
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
		
		HSSFRow rowhead = sheet.createRow(rowCounter);rowCounter++;
		rowhead.createCell(0).setCellValue("Sr. No.");
		rowhead.getCell(0).setCellStyle(columnHead);
		rowhead.createCell(1).setCellValue("Application ID");
		rowhead.getCell(1).setCellStyle(columnHead);
		rowhead.createCell(2).setCellValue("Customer ID");
		rowhead.getCell(2).setCellStyle(columnHead);
		/*rowhead.createCell(3).setCellValue("Loan ID");
		rowhead.getCell(3).setCellStyle(columnHead);*/
		rowhead.createCell(3).setCellValue("Session ID");
		rowhead.getCell(3).setCellStyle(columnHead);
		rowhead.createCell(4).setCellValue("App Creation Date");
		rowhead.getCell(4).setCellStyle(columnHead);
		rowhead.createCell(5).setCellValue("Rule Name");
		rowhead.getCell(5).setCellStyle(columnHead);
		rowhead.createCell(6).setCellValue("Model Name");
		rowhead.getCell(6).setCellStyle(columnHead);
		rowhead.createCell(7).setCellValue("Grade");
		rowhead.getCell(7).setCellStyle(columnHead);
		rowhead.createCell(8).setCellValue("Status");
		rowhead.getCell(8).setCellStyle(columnHead);
		rowhead.createCell(9).setCellValue("Status Date");
		rowhead.getCell(9).setCellStyle(columnHead);
		/*rowhead.createCell(11).setCellValue("App Funding Date");
		rowhead.getCell(11).setCellStyle(columnHead);*/
		
		for(int i=0;i<=9;i++)
			if(i==3 || i==5)sheet.setColumnWidth(i, 255*40);
			else sheet.setColumnWidth(i, 255*25);
		
		int dataCounter = 1;
		for(RulesViolatedData reportData :dailyRulesViolatedData){
			/*if(rowCounter==65536)
				break;*/
			HSSFRow row = sheet.createRow(rowCounter);rowCounter++;
			row.createCell(0).setCellValue(dataCounter);dataCounter++;
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(reportData.getApplicationId());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(reportData.getCustomerId());
	        row.getCell(2).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(reportData.getSessionId());
	        row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(4).setCellValue(reportData.getCreatedDate());
	        row.getCell(4).setCellStyle(rowStyle);
	        row.createCell(5).setCellValue(reportData.getRuleName());
	        row.getCell(5).setCellStyle(rowStyle);
	        row.createCell(6).setCellValue(reportData.getModelName());
	        row.getCell(6).setCellStyle(rowStyle);
	        row.createCell(7).setCellValue(reportData.getGrade());
	        row.getCell(7).setCellStyle(rowStyle);
	        row.createCell(8).setCellValue(reportData.getStatus());
	        row.getCell(8).setCellStyle(rowStyle);
	        row.createCell(9).setCellValue(reportData.getStatusDate());
	        row.getCell(9).setCellStyle(rowStyle);
		}
		return sheet;
	}
	
	public HSSFSheet weeklyRulesViolatedReport(List<RulesViolatedData> weeklyRulesViolatedData ,HSSFSheet sheet, HSSFWorkbook workbook){
		
		ArrayList<String> datesListForQuery = dateCalculator.dateCalcuateForWeeklyReport(formatter);
		int rowCounter = 0;
						
		HSSFRow header1 = sheet.createRow(rowCounter);rowCounter++;
	    header1.createCell(0).setCellValue("Weekly Rules Violated Report");
	   
	    HSSFRow header2 = sheet.createRow(rowCounter);rowCounter++;
	    header2.createCell(0).setCellValue("Report Period: " +datesListForQuery.get(0) + " to " + datesListForQuery.get(1));
	    
	    HSSFRow header3 = sheet.createRow(rowCounter);rowCounter++;
	    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
	    
	    header1.getCell(0).setCellStyle(headerStyle);
	    header2.getCell(0).setCellStyle(headerStyle);
	    header3.getCell(0).setCellStyle(headerStyle);
	   
	    HSSFRow header4 = sheet.createRow(rowCounter);rowCounter++;
	   
	    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,5);
	    sheet.addMergedRegion(cellRange1);
	    
	    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,5);
	    sheet.addMergedRegion(cellRange2);
	    
	    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,5);
	    sheet.addMergedRegion(cellRange3);
	    
	    short borderStyle = CellStyle.BORDER_MEDIUM;
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
		
		HSSFRow rowhead = sheet.createRow(rowCounter);rowCounter++;
		rowhead.createCell(0).setCellValue("Sr. No.");
		rowhead.getCell(0).setCellStyle(columnHead);
		rowhead.createCell(1).setCellValue("Application ID");
		rowhead.getCell(1).setCellStyle(columnHead);
		rowhead.createCell(2).setCellValue("Customer ID");
		rowhead.getCell(2).setCellStyle(columnHead);
		/*rowhead.createCell(3).setCellValue("Loan ID");
		rowhead.getCell(3).setCellStyle(columnHead);*/
		rowhead.createCell(3).setCellValue("Session ID");
		rowhead.getCell(3).setCellStyle(columnHead);
		rowhead.createCell(4).setCellValue("App Creation Date");
		rowhead.getCell(4).setCellStyle(columnHead);
		rowhead.createCell(5).setCellValue("Rule Name");
		rowhead.getCell(5).setCellStyle(columnHead);
		rowhead.createCell(6).setCellValue("Model Name");
		rowhead.getCell(6).setCellStyle(columnHead);
		rowhead.createCell(7).setCellValue("Grade");
		rowhead.getCell(7).setCellStyle(columnHead);
		rowhead.createCell(8).setCellValue("Status");
		rowhead.getCell(8).setCellStyle(columnHead);
		rowhead.createCell(9).setCellValue("Status Date");
		rowhead.getCell(9).setCellStyle(columnHead);
		/*rowhead.createCell(11).setCellValue("App Funding Date");
		rowhead.getCell(11).setCellStyle(columnHead);*/
		
		for(int i=0;i<=9;i++)
			if(i==3 || i==5)sheet.setColumnWidth(i, 255*40);
			else sheet.setColumnWidth(i, 255*25);
		
		int dataCounter = 1;
		
		for(RulesViolatedData reportData :weeklyRulesViolatedData){
			HSSFRow row = sheet.createRow(rowCounter);rowCounter++;
			row.createCell(0).setCellValue(dataCounter);dataCounter++;
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(reportData.getApplicationId());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(reportData.getCustomerId());
	        row.getCell(2).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(reportData.getSessionId());
	        row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(4).setCellValue(reportData.getCreatedDate());
	        row.getCell(4).setCellStyle(rowStyle);
	        row.createCell(5).setCellValue(reportData.getRuleName());
	        row.getCell(5).setCellStyle(rowStyle);
	        row.createCell(6).setCellValue(reportData.getModelName());
	        row.getCell(6).setCellStyle(rowStyle);
	        row.createCell(7).setCellValue(reportData.getGrade());
	        row.getCell(7).setCellStyle(rowStyle);
	        row.createCell(8).setCellValue(reportData.getStatus());
	        row.getCell(8).setCellStyle(rowStyle);
	        row.createCell(9).setCellValue(reportData.getStatusDate());
	        row.getCell(9).setCellStyle(rowStyle);
		}
		return sheet;
	}
	
	public HSSFSheet weeklySummaryReport(Map<String,RulesViolatedSummaryData> weeklyRulesViolatedSummaryData,HSSFSheet sheet,HSSFWorkbook workbook){
		
		int rowCounter = 0;
		
		ArrayList<String> datesListForQuery = dateCalculator.dateCalcuateForWeeklyReport(formatter);
		
		HSSFRow header1 = sheet.createRow(rowCounter);rowCounter++;
	    header1.createCell(0).setCellValue("Weekly Rules Violated Report");
	   
	    HSSFRow header2 = sheet.createRow(rowCounter);rowCounter++;
	    header2.createCell(0).setCellValue("Report Period: " +datesListForQuery.get(0) + " to " + datesListForQuery.get(1));
	    
	    HSSFRow header3 = sheet.createRow(rowCounter);rowCounter++;
	    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
	    
	    header1.getCell(0).setCellStyle(headerStyle);
	    header2.getCell(0).setCellStyle(headerStyle);
	    header3.getCell(0).setCellStyle(headerStyle);
	   
	    HSSFRow header4 = sheet.createRow(rowCounter);rowCounter++;
	   
	    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,6);
	    sheet.addMergedRegion(cellRange1);
	    
	    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,6);
	    sheet.addMergedRegion(cellRange2);
	    
	    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,6);
	    sheet.addMergedRegion(cellRange3);
	    
	    short borderStyle = CellStyle.BORDER_MEDIUM;
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
		
		HSSFRow rowhead = sheet.createRow(rowCounter);rowCounter++;
		rowhead.createCell(0).setCellValue("Sr. No.");
		rowhead.getCell(0).setCellStyle(columnHead);
		rowhead.createCell(1).setCellValue("Rule Name");
		rowhead.getCell(1).setCellStyle(columnHead);
		rowhead.createCell(2).setCellValue("# of Processed records");
		rowhead.getCell(2).setCellStyle(columnHead);
		rowhead.createCell(3).setCellValue("No. Approved");
		rowhead.getCell(3).setCellStyle(columnHead);
		rowhead.createCell(4).setCellValue("No. Declined");
		rowhead.getCell(4).setCellStyle(columnHead);
		rowhead.createCell(5).setCellValue("% Approved");
		rowhead.getCell(5).setCellStyle(columnHead);
		rowhead.createCell(6).setCellValue("% Declined");
		rowhead.getCell(6).setCellStyle(columnHead);
		
		for(int i=0;i<=6;i++)
			if(i==1)sheet.setColumnWidth(i, 255*40);
			else
				sheet.setColumnWidth(i, 255*25);
		
		int dataCounter = 1;
		DecimalFormat df = new DecimalFormat("#.00");
		Float passPercent = (float) 0;
		Float failPercent = (float) 0;
		Iterator<RulesViolatedSummaryData> it = weeklyRulesViolatedSummaryData.values().iterator();
		//for (String key: weeklyRulesViolatedSummaryData.keySet()) {
		while(it.hasNext()){
			RulesViolatedSummaryData reportData = it.next();
			Float total = (float) (reportData.getRuleFailCount()+reportData.getRulePassCount());
			passPercent = ((float)((float)reportData.getRulePassCount()/total))*100;
			failPercent = ((float)((float)reportData.getRuleFailCount()/total))*100;
			HSSFRow row = sheet.createRow(rowCounter);rowCounter++;
			row.createCell(0).setCellValue(dataCounter);dataCounter++;
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(reportData.getRuleName());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(reportData.getRuleFailCount()+reportData.getRulePassCount());
	        row.getCell(2).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(reportData.getRulePassCount());
	        row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(4).setCellValue(reportData.getRuleFailCount());
	        row.getCell(4).setCellStyle(rowStyle);
	        row.createCell(5).setCellValue(df.format(passPercent));
	        row.getCell(5).setCellStyle(rowStyle);
	        row.createCell(6).setCellValue(df.format(failPercent));
	        row.getCell(6).setCellStyle(rowStyle);
		}
		
		return sheet;
	}
	
	public HSSFSheet monthlyRulesViolatedReport(List<RulesViolatedData> monthlyRulesViolatedData ,HSSFSheet sheet, HSSFWorkbook workbook){
		
		int rowCounter = 0;
		
		ArrayList<String> datesListForQuery = dateCalculator.dateCalcuateForMonthlyReport(formatter);
		
		HSSFRow header1 = sheet.createRow(rowCounter);rowCounter++;
	    header1.createCell(0).setCellValue("Monthly Rules Violated Report");
	   
	    HSSFRow header2 = sheet.createRow(rowCounter);rowCounter++;
	    header2.createCell(0).setCellValue("Report Period: " +datesListForQuery.get(0) + " to " + datesListForQuery.get(1));
	    
	    HSSFRow header3 = sheet.createRow(rowCounter);rowCounter++;
	    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
	    
	    header1.getCell(0).setCellStyle(headerStyle);
	    header2.getCell(0).setCellStyle(headerStyle);
	    header3.getCell(0).setCellStyle(headerStyle);
	   
	    HSSFRow header4 = sheet.createRow(rowCounter);rowCounter++;
	   
	    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,5);
	    sheet.addMergedRegion(cellRange1);
	    
	    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,5);
	    sheet.addMergedRegion(cellRange2);
	    
	    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,5);
	    sheet.addMergedRegion(cellRange3);
	    
	    short borderStyle = CellStyle.BORDER_MEDIUM;
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
		
		HSSFRow rowhead = sheet.createRow(rowCounter);rowCounter++;
		rowhead.createCell(0).setCellValue("Sr. No.");
		rowhead.getCell(0).setCellStyle(columnHead);
		rowhead.createCell(1).setCellValue("Application ID");
		rowhead.getCell(1).setCellStyle(columnHead);
		rowhead.createCell(2).setCellValue("Customer ID");
		rowhead.getCell(2).setCellStyle(columnHead);
		/*rowhead.createCell(3).setCellValue("Loan ID");
		rowhead.getCell(3).setCellStyle(columnHead);*/
		rowhead.createCell(3).setCellValue("Session ID");
		rowhead.getCell(3).setCellStyle(columnHead);
		rowhead.createCell(4).setCellValue("App Creation Date");
		rowhead.getCell(4).setCellStyle(columnHead);
		rowhead.createCell(5).setCellValue("Rule Name");
		rowhead.getCell(5).setCellStyle(columnHead);
		rowhead.createCell(6).setCellValue("Model Name");
		rowhead.getCell(6).setCellStyle(columnHead);
		rowhead.createCell(7).setCellValue("Grade");
		rowhead.getCell(7).setCellStyle(columnHead);
		rowhead.createCell(8).setCellValue("Status");
		rowhead.getCell(8).setCellStyle(columnHead);
		rowhead.createCell(9).setCellValue("Status Date");
		rowhead.getCell(9).setCellStyle(columnHead);
		/*rowhead.createCell(11).setCellValue("App Funding Date");
		rowhead.getCell(11).setCellStyle(columnHead);*/
		
		for(int i=0;i<=9;i++)
			if(i==3 || i==5)sheet.setColumnWidth(i, 255*40);
			else sheet.setColumnWidth(i, 255*25);
		
		int dataCounter = 1;
		for(RulesViolatedData reportData :monthlyRulesViolatedData){
			HSSFRow row = sheet.createRow(rowCounter);rowCounter++;
			row.createCell(0).setCellValue(dataCounter);dataCounter++;
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(reportData.getApplicationId());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(reportData.getCustomerId());
	        row.getCell(2).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(reportData.getSessionId());
	        row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(4).setCellValue(reportData.getCreatedDate());
	        row.getCell(4).setCellStyle(rowStyle);
	        row.createCell(5).setCellValue(reportData.getRuleName());
	        row.getCell(5).setCellStyle(rowStyle);
	        row.createCell(6).setCellValue(reportData.getModelName());
	        row.getCell(6).setCellStyle(rowStyle);
	        row.createCell(7).setCellValue(reportData.getGrade());
	        row.getCell(7).setCellStyle(rowStyle);
	        row.createCell(8).setCellValue(reportData.getStatus());
	        row.getCell(8).setCellStyle(rowStyle);
	        row.createCell(9).setCellValue(reportData.getStatusDate());
	        row.getCell(9).setCellStyle(rowStyle);
		}
		return sheet;
	}
	
	public HSSFSheet monthlySummaryReport(Map<String,RulesViolatedSummaryData> monthlyRulesViolatedSummaryData,HSSFSheet sheet,HSSFWorkbook workbook){
		
		int rowCounter = 0;
		
		ArrayList<String> datesListForQuery = dateCalculator.dateCalcuateForMonthlyReport(formatter);
		
		HSSFRow header1 = sheet.createRow(rowCounter);rowCounter++;
	    header1.createCell(0).setCellValue("Monthly Rules Violated Report");
	   
	    HSSFRow header2 = sheet.createRow(rowCounter);rowCounter++;
	    header2.createCell(0).setCellValue("Report Period: " +datesListForQuery.get(0) + " to " + datesListForQuery.get(1));
	    
	    HSSFRow header3 = sheet.createRow(rowCounter);rowCounter++;
	    header3.createCell(0).setCellValue("Creation Date: "+formatter.format(new Date()));
	    
	    header1.getCell(0).setCellStyle(headerStyle);
	    header2.getCell(0).setCellStyle(headerStyle);
	    header3.getCell(0).setCellStyle(headerStyle);
	   
	    HSSFRow header4 = sheet.createRow(rowCounter);rowCounter++;
	   
	    CellRangeAddress cellRange1 = new CellRangeAddress(0,0,0,6);
	    sheet.addMergedRegion(cellRange1);
	    
	    CellRangeAddress cellRange2 = new CellRangeAddress(1,1,0,6);
	    sheet.addMergedRegion(cellRange2);
	    
	    CellRangeAddress cellRange3 = new CellRangeAddress(2,2,0,6);
	    sheet.addMergedRegion(cellRange3);
	    
	    short borderStyle = CellStyle.BORDER_MEDIUM;
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange1, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange1, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange2, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange2, sheet, workbook);
	    
	    RegionUtil.setBorderBottom(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderTop(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderLeft(borderStyle, cellRange3, sheet, workbook);
	    RegionUtil.setBorderRight(borderStyle, cellRange3, sheet, workbook);
		
		HSSFRow rowhead = sheet.createRow(rowCounter);rowCounter++;
		rowhead.createCell(0).setCellValue("Sr. No.");
		rowhead.getCell(0).setCellStyle(columnHead);
		rowhead.createCell(1).setCellValue("Rule Name");
		rowhead.getCell(1).setCellStyle(columnHead);
		rowhead.createCell(2).setCellValue("# of Processed records");
		rowhead.getCell(2).setCellStyle(columnHead);
		rowhead.createCell(3).setCellValue("No. Approved");
		rowhead.getCell(3).setCellStyle(columnHead);
		rowhead.createCell(4).setCellValue("No. Declined");
		rowhead.getCell(4).setCellStyle(columnHead);
		rowhead.createCell(5).setCellValue("% Approved");
		rowhead.getCell(5).setCellStyle(columnHead);
		rowhead.createCell(6).setCellValue("% Declined");
		rowhead.getCell(6).setCellStyle(columnHead);
		
		for(int i=0;i<=6;i++)
			if(i==1)sheet.setColumnWidth(i, 255*40);
			else
				sheet.setColumnWidth(i, 255*25);
		
		int dataCounter = 1;
		DecimalFormat df = new DecimalFormat("#.00");
		Float passPercent = (float) 0;
		Float failPercent = (float) 0;
		Float total = (float) 0; 
		Iterator<RulesViolatedSummaryData> it = monthlyRulesViolatedSummaryData.values().iterator();
		while(it.hasNext()){
			RulesViolatedSummaryData reportData = it.next();
			total = (float) (reportData.getRuleFailCount()+reportData.getRulePassCount());
			passPercent = ((float)((float)reportData.getRulePassCount()/total))*100;
			failPercent = ((float)((float)reportData.getRuleFailCount()/total))*100;
			HSSFRow row = sheet.createRow(rowCounter);rowCounter++;
			row.createCell(0).setCellValue(dataCounter);dataCounter++;
	        row.getCell(0).setCellStyle(rowStyle);
	        row.createCell(1).setCellValue(reportData.getRuleName());
	        row.getCell(1).setCellStyle(rowStyle);
	        row.createCell(2).setCellValue(reportData.getRuleFailCount()+reportData.getRulePassCount());
	        row.getCell(2).setCellStyle(rowStyle);
	        row.createCell(3).setCellValue(reportData.getRulePassCount());
	        row.getCell(3).setCellStyle(rowStyle);
	        row.createCell(4).setCellValue(reportData.getRuleFailCount());
	        row.getCell(4).setCellStyle(rowStyle);
	        row.createCell(5).setCellValue(df.format(passPercent));
	        row.getCell(5).setCellStyle(rowStyle);
	        row.createCell(6).setCellValue(df.format(failPercent));
	        row.getCell(6).setCellStyle(rowStyle);
		}
		
		return sheet;
	}
}
