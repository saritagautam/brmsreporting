package com.lendinpoint;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import com.lendinpoint.common.DateCalculator;
import com.lendinpoint.common.ReportMailSender;
import com.lendinpoint.job.processor.DurationReportJobProcessor;
import com.lendinpoint.job.processor.ErrorReportJobProcessor;
import com.lendinpoint.job.processor.ReconcilationJobProcessor;
import com.lendinpoint.job.processor.RulesViolatedJobProcessor;
import com.lendinpoint.service.ConfigService;

@Configuration
@EnableScheduling
@Component
public class CronJob {
	
	@Autowired
	ReconcilationJobProcessor reconcilationJobProcessor;
	
	@Autowired
	ErrorReportJobProcessor errorReportJobProcessor;
	
	@Autowired
	RulesViolatedJobProcessor rulesViolatedJobProcessor;
	
	@Autowired
	DurationReportJobProcessor durationReportJobProcessor;
	
	@Autowired
	ConfigService configService;
	
	@Autowired
	ReportMailSender reportMailSender;
	
	@Autowired
	DateCalculator dateCalculator;
	
	Logger logger = LoggerFactory.getLogger(CronJob.class);
	
	public void run()
    {
		String reconcilationFile = null;
		String errorReportFile = null;
		String rulesViolatedReportFile = null;
		String durationReportFile = null;
		
		SimpleDateFormat creditReportIntervalDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		
		String mailTo = configService.getConfigByName("mailTo");
		String mailBcc = configService.getConfigByName("mailBcc");
		Calendar cal = Calendar.getInstance();
		
		if(cal.get(Calendar.DAY_OF_MONTH) == 1)//generate reconcilation report monthly
		{
			logger.debug("**************** Running monthly Reconcilation report ******************");
			try{
				ArrayList<String> datesListForQuery=dateCalculator.dateCalcuateForMonthlyReport(creditReportIntervalDateFormat);
				reconcilationFile = reconcilationJobProcessor.generateReconcileReport(datesListForQuery);
			}catch(Exception e){
				logger.error("Exception in reconcilation report");
				StringWriter sw = new StringWriter();
		        e.printStackTrace(new PrintWriter(sw));
				logger.error("Exception : "+sw.toString());
			}
			logger.debug("reconcilationFile : " + reconcilationFile);
		}
		
		logger.debug("**************** Running Error report ******************");
		try{
			errorReportFile = errorReportJobProcessor.generateErrorReport();
		}catch(Exception e){
			logger.error("Exception in error report");
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("Exception : "+sw.toString());
		}
		logger.debug("errorReportFile : "+errorReportFile);
		
		logger.debug("**************** Running Rules report ******************");
		try{
			rulesViolatedReportFile = rulesViolatedJobProcessor.generateRulesViolatedReport();
			logger.debug("rulesViolatedReportFile : "+rulesViolatedReportFile);
		}catch(Exception e){
			logger.error("Exception in rules violated report");
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("Exception : "+sw.toString());
		}
		
		logger.debug("**************** Running Duration report ******************");
		try{
			durationReportFile = durationReportJobProcessor.generateDurationReport();
			logger.debug("durationReportFile : "+durationReportFile);
		}catch(Exception e){
			logger.error("Exception in duration report");
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("Exception : "+sw.toString());
		}
		
		if(reconcilationFile != null){
			reportMailSender.sendEmail(reconcilationFile, mailTo, mailBcc, "reconcile");
		}
		if(durationReportFile != null){
			reportMailSender.sendEmail(durationReportFile, mailTo, mailBcc, "duration");
		}
		if(errorReportFile != null){
			reportMailSender.sendEmail(errorReportFile, mailTo, mailBcc, "error");
		}
		if(rulesViolatedReportFile != null){
			reportMailSender.sendEmail(rulesViolatedReportFile, mailTo, mailBcc, "rules");
		}
		//System.exit(0);
    }
	
	
}
