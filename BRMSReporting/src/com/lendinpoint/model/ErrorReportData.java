package com.lendinpoint.model;

public class ErrorReportData {

	private String error;
	private String errorDate;
	private String applicationId;
	private String customerId;
	private String loanId;
	private String sessionId;
	private String contractStatus;
	private String fsAssigned;
	private String csAssigned;
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getErrorDate() {
		return errorDate;
	}
	public void setErrorDate(String errorDate) {
		this.errorDate = errorDate;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getLoanId() {
		return loanId;
	}
	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getContractStatus() {
		return contractStatus;
	}
	public void setContractStatus(String contractStatus) {
		this.contractStatus = contractStatus;
	}
	public String getFsAssigned() {
		return fsAssigned;
	}
	public void setFsAssigned(String fsAssigned) {
		this.fsAssigned = fsAssigned;
	}
	public String getCsAssigned() {
		return csAssigned;
	}
	public void setCsAssigned(String csAssigned) {
		this.csAssigned = csAssigned;
	}
	
}
