package com.lendinpoint.model;

public class ReconcilationData {
	private String applicationId;
	private String customerId;
	private String loanId;
	private String sessionId;
	private String attemptsCount;
	
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getLoanId() {
		return loanId;
	}
	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}
	public String getAttemptsCount() {
		return attemptsCount;
	}
	public void setAttemptsCount(String attemptsCount) {
		this.attemptsCount = attemptsCount;
	}
}
