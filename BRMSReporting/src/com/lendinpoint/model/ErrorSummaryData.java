package com.lendinpoint.model;

public class ErrorSummaryData {
	private String error;
	private int count;
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	} 
}
