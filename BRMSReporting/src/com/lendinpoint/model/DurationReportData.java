package com.lendinpoint.model;

import java.sql.Timestamp;

public class DurationReportData implements Comparable<DurationReportData>{
	
	private String applicationId;
	private String customerId;
	private String loanId;
	private String sessionId;
	private Timestamp startTime;
	private Timestamp stopTime;
	private Integer elapsedTime;
	
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getLoanId() {
		return loanId;
	}
	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public Integer getElapsedTime() {
		return elapsedTime;
	}
	public void setElapsedTime(Integer elapsedTime) {
		this.elapsedTime = elapsedTime;
	}
	public Timestamp getStartTime() {
		return startTime;
	}
	public void setStartTime(Timestamp timestamp) {
		this.startTime = timestamp;
	}
	public Timestamp getStopTime() {
		return stopTime;
	}
	public void setStopTime(Timestamp stopTime) {
		this.stopTime = stopTime;
	}
	@Override
	public int compareTo(DurationReportData other) {
		return elapsedTime.compareTo(other.elapsedTime);
	}
	
}
