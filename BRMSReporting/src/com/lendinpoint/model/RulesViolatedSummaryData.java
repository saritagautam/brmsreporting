package com.lendinpoint.model;

public class RulesViolatedSummaryData {
	private String ruleName;
	private Integer rulePassCount;
	private Integer ruleFailCount;
	
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	
	public Integer getRulePassCount() {
		return rulePassCount;
	}
	public void setRulePassCount(Integer rulePassCount) {
		this.rulePassCount = rulePassCount;
	}
	public Integer getRuleFailCount() {
		return ruleFailCount;
	}
	public void setRuleFailCount(Integer ruleFailCount) {
		this.ruleFailCount = ruleFailCount;
	}
}
