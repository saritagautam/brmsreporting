package com.lendinpoint.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="config")
public class Config {
	private Long id;
	private String configName;
	private String configValue;
	
	@Id
    @GeneratedValue
    @Column(name = "id", columnDefinition = "bigint(20)")
	public Long getId() {
		return id;
	}
	public String getConfigName() {
		return configName;
	}
	public void setConfigName(String configName) {
		this.configName = configName;
	}
	public String getConfigValue() {
		return configValue;
	}
	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}
	public void setId(Long id) {
		this.id = id;
	}
}

