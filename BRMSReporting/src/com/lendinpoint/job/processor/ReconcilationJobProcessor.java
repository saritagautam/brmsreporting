package com.lendinpoint.job.processor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.lendinpoint.data.processor.ReconcilationProcessor;
import com.lendinpoint.model.ReconcilationData;
import com.lendinpoint.report.processor.ReconcilationReportProcessor;

public class ReconcilationJobProcessor {
	
	@Autowired
	ReconcilationProcessor reconcilationProcessor;
	
	@Autowired
	ReconcilationReportProcessor reconcilationReportProcessor;
	
	Logger logger = LoggerFactory.getLogger(ReconcilationJobProcessor.class);
	
	public String generateReconcileReport(ArrayList<String> datesListForQuery){
		String fileName = null;
		try
		{ 
			List<ReconcilationData> totalGDSRequestData = reconcilationProcessor.totalGDSRequest(datesListForQuery);
			logger.debug("totalGDSRequestData " + totalGDSRequestData.size());
			List<ReconcilationData> gdsRequestFirstTimeData = reconcilationProcessor.totalGDSRequestFirstTime(datesListForQuery);
			logger.debug("gdsRequestFirstTimeData " + gdsRequestFirstTimeData.size());
			List<ReconcilationData> gdsRequestMoreThanOnceData = reconcilationProcessor.gdsRequestsMoreThanOnce(datesListForQuery);
			logger.debug("gdsRequestMoreThanOnceData " + gdsRequestMoreThanOnceData.size());
			List<ReconcilationData> gdsRequestsErroredOutData = reconcilationProcessor.gdsRequestsErroredOut(datesListForQuery);
			logger.debug("gdsRequestsErroredOut " + gdsRequestsErroredOutData.size());
			List<ReconcilationData> gdsRequestsSentFirstTimeErroredOutData = reconcilationProcessor.gdsRequestsSentFirstTimeErroredOut(datesListForQuery);
			logger.debug("gdsRequestsSentFirstTimeErroredOutData " + gdsRequestsSentFirstTimeErroredOutData.size());
			List<ReconcilationData> gdsRequestSentMoreThanOnceErroredOutData = reconcilationProcessor.gdsRequestSentMoreThanOnceErroredOut(datesListForQuery);
			logger.debug("gdsRequestSentMoreThanOnceErroredOutData " + gdsRequestSentMoreThanOnceErroredOutData.size());	
					
			reconcilationProcessor.closeConnection();
			
			fileName = reconcilationReportProcessor.reportProcessor(
					totalGDSRequestData, gdsRequestFirstTimeData,
					gdsRequestMoreThanOnceData, gdsRequestsErroredOutData,
					gdsRequestsSentFirstTimeErroredOutData,gdsRequestSentMoreThanOnceErroredOutData);
			
			
		}catch(Exception ex){
			
			StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            logger.debug("Exception in generateReconcileReport : "+ex);
		}
		return fileName;
	}
}
