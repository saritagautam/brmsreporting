package com.lendinpoint.job.processor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.lendinpoint.common.ReportMailSender;
import com.lendinpoint.data.processor.DurationReportDataProcessor;
import com.lendinpoint.model.DurationReportData;
import com.lendinpoint.report.processor.DurationReportProcessor;

public class DurationReportJobProcessor {
	
	@Autowired
	DurationReportDataProcessor durationReportDataProcessor;
	
	@Autowired
	DurationReportProcessor durationReportProcessor;
	
	Logger logger = LoggerFactory.getLogger(DurationReportJobProcessor.class);
	
	int weekDays = 7;
	int monthDays= 30;
	
	public String generateDurationReport(){
		Calendar date = Calendar.getInstance();
		Boolean generateWeeklyreport = false;
		Boolean generateMonthlyReport = false;
		if(date.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
			generateWeeklyreport = true;
		if(date.get(Calendar.DAY_OF_MONTH) == 1)
			generateMonthlyReport = true;
		 
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		cal.set(Calendar.DATE, 1);
		int days = cal.getActualMaximum(Calendar.DATE);
		logger.debug("days : "+days);
		monthDays = days;
		
		String fileName = null;
		
		List<DurationReportData> dailyDurationReportData = new ArrayList<DurationReportData>();
		List<DurationReportData> weeklyDurationReportData = new ArrayList<DurationReportData>();
		List<DurationReportData> monthlyDurationReportData = new ArrayList<DurationReportData>();
		try{
			dailyDurationReportData = durationReportDataProcessor.durationReport(1);
			logger.debug("dailyDurationReportData : "+dailyDurationReportData.size());
			Collections.sort(dailyDurationReportData);
			if(generateWeeklyreport){
				weeklyDurationReportData = durationReportDataProcessor.durationReport(weekDays);
				Collections.sort(weeklyDurationReportData);
				logger.debug("weeklyDurationReportData : "+weeklyDurationReportData.size());
			}
			if(generateMonthlyReport){
				monthlyDurationReportData = durationReportDataProcessor.durationReport(monthDays);
				Collections.sort(monthlyDurationReportData);
				logger.debug("monthlyDurationReportData : "+monthlyDurationReportData.size());
			}
			
			durationReportDataProcessor.closeConnection();
			
			fileName = durationReportProcessor.processDurationReportData(
					dailyDurationReportData, weeklyDurationReportData,
					monthlyDurationReportData);
			
		}catch(Exception e){
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("Exception in Duration Report Job processor "+sw.toString());
		}
		return fileName;
	}

}
