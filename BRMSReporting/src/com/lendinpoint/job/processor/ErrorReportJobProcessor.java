package com.lendinpoint.job.processor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.lendinpoint.data.processor.ErrorDataProcessor;
import com.lendinpoint.model.ErrorReportData;
import com.lendinpoint.model.ErrorSummaryData;
import com.lendinpoint.report.processor.ErrorReportProcessor;

public class ErrorReportJobProcessor {

	@Autowired
	ErrorDataProcessor errorDataProcessor;

	@Autowired
	ErrorReportProcessor errorReportProcessor;

	int weekDays = 7;
	int monthDays= 30;
	
	Logger logger = LoggerFactory.getLogger(ErrorReportJobProcessor.class);
	
	public String generateErrorReport(){

		 Calendar date = Calendar.getInstance();
		 Boolean generateWeeklyreport = false;
		 Boolean generateMonthlyReport = false;
		 if(date.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
			 generateWeeklyreport = true;
		 if(date.get(Calendar.DAY_OF_MONTH) == 1)
			 generateMonthlyReport = true;
		 
		 Calendar cal = Calendar.getInstance();
		 cal.add(Calendar.MONTH, -1);
		 cal.set(Calendar.DATE, 1);
		 int days = cal.getActualMaximum(Calendar.DATE);
		 logger.debug("Number of days in previous month: "+days);
		 monthDays = days;
		 
		 String fileName = null;
		 Boolean killConnection = false;
		try {
			List<ErrorReportData> weeklyErrorReportData = new ArrayList<ErrorReportData>();
			List<ErrorSummaryData> dailySummaryData = new ArrayList<ErrorSummaryData>();
			List<ErrorSummaryData> weeklySummaryData = new ArrayList<ErrorSummaryData>();
			List<ErrorReportData> monthlyErrorReportData = new ArrayList<ErrorReportData>();
			List<ErrorSummaryData> monthlySummaryData = new ArrayList<ErrorSummaryData>();
			int dailyTotalRequests = 1;
			int weeklyTotalRequests = 1;
			int monthlyTotalRequests= 1;
			List<ErrorReportData> dailyErrorReportData = errorDataProcessor.errorReport(1);
			logger.debug("dailyErrorReportData "+ dailyErrorReportData.size());
			dailySummaryData = errorDataProcessor.summaryData(1);
			logger.debug("datilyErrorSummaryData :"+dailySummaryData.size());
			dailyTotalRequests = errorDataProcessor.totalRequests(1);
			logger.debug("dailyTotalRequests :"+dailyTotalRequests);
			if(generateWeeklyreport){
				weeklyErrorReportData = errorDataProcessor.errorReport(weekDays);
				logger.debug("weeklyErrorReportData : "+weeklyErrorReportData.size());
				weeklySummaryData = errorDataProcessor.summaryData(weekDays);
				logger.debug("weeklySummaryData : "+weeklySummaryData.size());
				weeklyTotalRequests = errorDataProcessor.totalRequests(weekDays);
				logger.debug("weeklyTotalRequests : " +weeklyTotalRequests);
			}
			if(generateMonthlyReport){
				monthlyErrorReportData = errorDataProcessor.errorReport(monthDays);
				logger.debug("monthlyErrorReportData : "+monthlyErrorReportData.size());
				monthlySummaryData = errorDataProcessor.summaryData(monthDays);
				logger.debug("monthlySummaryData : "+monthlySummaryData.size());
				monthlyTotalRequests = errorDataProcessor.totalRequests(monthDays);
				logger.debug("monthlyTotalRequests : "+monthlyTotalRequests);
			}
			
			errorDataProcessor.closeConnection();
			
			fileName = errorReportProcessor.processErrorReportData(
					dailyErrorReportData,dailySummaryData,dailyTotalRequests, weeklyErrorReportData,
					weeklySummaryData, weeklyTotalRequests,monthlyErrorReportData,
					monthlySummaryData,monthlyTotalRequests);
			
		} catch (Exception e) {
			logger.error("Exception in generateErrorReport : "+e);
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("Exception : "+sw.toString());
		}
		return fileName;
	}
}
