package com.lendinpoint.job.processor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.lendinpoint.data.processor.RulesViolatedDataProcessor;
import com.lendinpoint.model.RulesViolatedData;
import com.lendinpoint.model.RulesViolatedSummaryData;
import com.lendinpoint.report.processor.RulesViolatedReportProcessor;

public class RulesViolatedJobProcessor {
	@Autowired
	RulesViolatedReportProcessor rulesViolatedReportProcessor;
	
	@Autowired
	RulesViolatedDataProcessor rulesViolatedDataProcessor;
	
	Logger logger = LoggerFactory.getLogger(RulesViolatedJobProcessor.class);
	
	int weekDays = 7;
	int monthDays= 30;
	
	public String generateRulesViolatedReport(){
		
		Calendar date = Calendar.getInstance();
		Boolean generateWeeklyreport = false;
		Boolean generateMonthlyReport = false;
		if(date.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
			generateWeeklyreport = true;
		if(date.get(Calendar.DAY_OF_MONTH) == 1)
			generateMonthlyReport = true;
		 
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		cal.set(Calendar.DATE, 1);
		int days = cal.getActualMaximum(Calendar.DATE);
		System.out.println("days : "+days);
		monthDays = days;
		
		String fileName = null;
		List<RulesViolatedData> dailyRulesViolatedData  = new ArrayList<RulesViolatedData>();
		LinkedHashMap<String, RulesViolatedSummaryData> dailyRulesViolatedSummaryData = new LinkedHashMap<String, RulesViolatedSummaryData>();
		List<RulesViolatedData> weeklyRulesViolatedData  = new ArrayList<RulesViolatedData>();
		LinkedHashMap<String, RulesViolatedSummaryData> weeklyRulesViolatedSummaryData = new LinkedHashMap<String, RulesViolatedSummaryData>();
		List<RulesViolatedData> monthlyRulesViolatedData  = new ArrayList<RulesViolatedData>();
		LinkedHashMap<String, RulesViolatedSummaryData> monthlyRulesViolatedSummaryData = new LinkedHashMap<String, RulesViolatedSummaryData>();
		try
		{ 
			dailyRulesViolatedData = rulesViolatedDataProcessor.rulesViolated(1);
			logger.debug("dailyRulesViolatedData : "+dailyRulesViolatedData.size());
			dailyRulesViolatedSummaryData = rulesViolatedDataProcessor.rulesViolatedSummary(1);
			logger.debug("dailyRulesViolatedSummaryData : "+dailyRulesViolatedSummaryData.size());
			if(generateWeeklyreport){
				weeklyRulesViolatedData = rulesViolatedDataProcessor.rulesViolated(weekDays);
				logger.debug("weeklyRulesViolatedData : "+weeklyRulesViolatedData.size());
				weeklyRulesViolatedSummaryData = rulesViolatedDataProcessor.rulesViolatedSummary(weekDays);
				logger.debug("weeklyRulesViolatedSummaryData : "+weeklyRulesViolatedSummaryData.size());
			}
			if(generateMonthlyReport){
				monthlyRulesViolatedData = rulesViolatedDataProcessor.rulesViolated(monthDays);
				logger.debug("monthlyRulesViolatedData : "+monthlyRulesViolatedData.size());
				monthlyRulesViolatedSummaryData = rulesViolatedDataProcessor.rulesViolatedSummary(monthDays);
				logger.debug("monthlyRulesViolatedSummaryData : "+monthlyRulesViolatedSummaryData.size());
			}
			
			rulesViolatedDataProcessor.closeConnection();
			
			fileName = rulesViolatedReportProcessor
					.processRulesViolatedReportData(dailyRulesViolatedData,
							dailyRulesViolatedSummaryData,
							weeklyRulesViolatedData,
							weeklyRulesViolatedSummaryData,
							monthlyRulesViolatedData,
							monthlyRulesViolatedSummaryData);
		}catch(Exception e){
			logger.error("error in rules violated Job Processor ");
            StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("Exception : "+sw.toString());
			
		}
		return fileName;
	}
}
