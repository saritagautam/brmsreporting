package com.lendinpoint.common;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.hadoop.security.UserGroupInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HiveConnector {
	private static final String CONF_KERBEROS_KEY = "hadoop.security.authentication";
	private static final String CONF_KERBEROS_VALUE = "Kerberos";
	//For Prod
	private static final String HIVE_USER_PRINCIPAL = "hive/ip-172-42-50-39.lendingpoint.ad@LPANALYTICS.HDP";
	private static final String HIVE_CONNECTION_STRING = "jdbc:hive2://52.204.138.8:10001/;principal=hive/ip-172-42-50-39.lendingpoint.ad@LPANALYTICS.HDP;transportMode=http;httpPath=cliservice;auth=kerberos;saslQop=auth-conf";
	private static final String HIVE_USER_KEYTAB = "/etc/security/keytabs/hive.service.keytab";
	//For UAT
	//private static final String HIVE_USER_PRINCIPAL = "hive/ip-172-42-90-11.lendingpoint.ad@LPQA.HDP";
	//private static final String HIVE_USER_KEYTAB = "/etc/security/keytabs/hive.service.keytab";
	//for local
	//private static final String HIVE_USER_KEYTAB = "C:\\Users\\sarita.gautam\\Desktop\\LendingPoint\\Hive\\conf_qa\\hive.qa.keytab";
	//private static final String HIVE_CONNECTION_STRING = "jdbc:hive2://34.196.201.199:10000/;principal=hive/ip-172-42-90-11.lendingpoint.ad@LPQA.HDP";
	private static final String HIVE_JDBC_DRIVER = "org.apache.hive.jdbc.HiveDriver";
	
	Logger logger = LoggerFactory.getLogger(HiveConnector.class);
	
	public Connection getHiveConnction(){
		org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
        conf.set(CONF_KERBEROS_KEY, CONF_KERBEROS_VALUE);
        UserGroupInformation.setConfiguration(conf);
        Connection hiveConnection = null;
        try {
			UserGroupInformation.loginUserFromKeytab(HIVE_USER_PRINCIPAL, HIVE_USER_KEYTAB);
			Class.forName(HIVE_JDBC_DRIVER);
			
			hiveConnection = DriverManager.getConnection(HIVE_CONNECTION_STRING);
			logger.debug("got hiveConnection");
		} catch (IOException e) {
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("Exception in hive connector "+sw.toString());
		}catch (ClassNotFoundException e) {
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("Exception in hive connector "+sw.toString());
		} catch (SQLException e) {
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("Exception in hive connector "+sw.toString());
		}
        
        return hiveConnection;
	}
}
