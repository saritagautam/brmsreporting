package com.lendinpoint.common;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

public class ReportMailSender {

	@Autowired
	JavaMailSender mailSender;
	
	@Value("${mail.from}")
    private String MAIL_FROM;
	
	Logger logger = LoggerFactory.getLogger(ReportMailSender.class);
	
	public String sendEmail(String fileName,String mailTo,String mailBcc,String reportType){
		String status = "200";
		try {
			
			//End of month BRMS reconciliation report
			logger.debug("in mail sender");
			
			//convert UTC to EST date
			Date date = new Date();
			DateFormat formatterEST = new SimpleDateFormat("yyyyMMdd");
			DateFormat formatterESTSUB = new SimpleDateFormat("MM/dd/yyyy");
			formatterEST.setTimeZone(TimeZone.getTimeZone("EST")); // UTC timezone
			String estDate = formatterEST.format(date);
			String dateforSub = formatterESTSUB.format(date);
			
			String[] sendBcc = null;
			if(mailBcc != null && mailBcc.length()>1)
				sendBcc=mailBcc.split(",");
            String body = "";
            
            MimeMessage msg = mailSender.createMimeMessage();
            
            MimeMessageHelper helper = new MimeMessageHelper(msg, true);
            helper.setFrom(this.MAIL_FROM);
            helper.setTo(mailTo);
            if(sendBcc != null)
            	helper.setBcc(sendBcc);
            
            FileSystemResource file = new FileSystemResource(fileName);
    		//helper.addAttachment(file.getFilename(), new ByteArrayResource(content));
    		helper.addAttachment(file.getFilename(), file);
    		if(reportType.equalsIgnoreCase("reconcile")){
    			 msg.setSubject("End of month BRMS Reconciliation Report "+dateforSub);
    			 body="Hi Team,<br/><br/>Please find attached BRMS Reconcilation report.<br/><br/>Thanks,<br/>IT Team";
    		}
    		else if(reportType.equalsIgnoreCase("error")){
    			msg.setSubject("BRMS Error Report "+dateforSub);
    			body="Hi Team,<br/><br/>Please find attached BRMS Error report.<br/><br/>Thanks,<br/>IT Team";
    		}
    		else if(reportType.equalsIgnoreCase("rules")){
    			msg.setSubject("BRMS Rules Violated Report "+dateforSub);
    			body="Hi Team,<br/><br/>Please find attached BRMS Rules Violated report.<br/><br/>Thanks,<br/>IT Team";
    		}
    		else if(reportType.equalsIgnoreCase("duration")){
    			msg.setSubject("BRMS Duration Report "+dateforSub);
    			body="Hi Team,<br/><br/>Please find attached BRMS Duration report.<br/><br/>Thanks,<br/>IT Team";
    		}
            //helper.setText(body, true);;
            
            //set body
            helper.setText(body, true);
           
			
            mailSender.send(msg);
        } catch (MailException ex) {
        	logger.error("MailException : "+ex);
            status = "Exception in sending Mail :"+ex;
            StringWriter sw = new StringWriter();
	        ex.printStackTrace(new PrintWriter(sw));
			logger.error("Exception : "+sw.toString());
        } catch (MessagingException e) {
        	logger.error("MessagingException : "+e);
        	status = "Exception in sending Mail :"+e;
        	StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("Exception : "+sw.toString());
		}catch(Exception ex){
			logger.error("Exception in sending email : "+ex);
			status = "Exception in sending Mail :"+ex;
			StringWriter sw = new StringWriter();
	        ex.printStackTrace(new PrintWriter(sw));
			logger.error("Exception : "+sw.toString());
		}
		logger.debug("Mail sending done");
		return status;
	}
}
