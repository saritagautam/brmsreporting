package com.lendinpoint.common;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class DateCalculator {
	public ArrayList<String> dateCalcuateForMonthlyReport(SimpleDateFormat dateFormatter){
		ArrayList<String> dateList = new ArrayList<String>();
	    String startDate = null;
	    String endDate = null;
	    
	    Calendar calendar = Calendar.getInstance();
	    calendar.add(Calendar.MONTH, -1);
	    calendar.set(Calendar.DATE, 1);
	    
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    //add 4 hrs to solve hive timezone issue
	    //calendar.add(Calendar.HOUR_OF_DAY, 4);
	    
	    startDate = dateFormatter.format(calendar.getTime()).toString();
	    //System.out.println("startDate is" + startDate);
	    int ReportInterval = calendar.getActualMaximum(Calendar.DATE);
	   
	   // System.out.println("ReportInterval is: "+ReportInterval);
	    calendar.add(Calendar.DATE, ReportInterval-1);
	   
	    calendar.set(Calendar.HOUR_OF_DAY, 23);
	    calendar.set(Calendar.MINUTE, 59);
	    calendar.set(Calendar.SECOND, 59);
	    calendar.set(Calendar.MILLISECOND, 999);
	    //add 4 hrs to solve hive timezone issue
	    //calendar.add(Calendar.HOUR_OF_DAY, 4);
	    
	    endDate = dateFormatter.format(calendar.getTime()).toString();
	   // System.out.println("endDate is " + endDate);
	    dateList.add(startDate);
	    dateList.add(endDate);
	    return dateList;
		
	}
	
	public ArrayList<String> dateCalcuateForWeeklyReport(SimpleDateFormat dateFormatter){
		ArrayList<String> dateList = new ArrayList<String>();
	    String startDate = null;
	    String endDate = null;
	    
	    Calendar calendar = Calendar.getInstance();
	    calendar.add(Calendar.WEEK_OF_YEAR, -1);
	    calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek()+1);
	    
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    //add 4 hrs to solve hive timezone issue
	    //calendar.add(Calendar.HOUR_OF_DAY, 4);
	    
	    startDate = dateFormatter.format(calendar.getTime()).toString();
	    System.out.println("Week startDate is" + startDate);
	    
	    calendar = (Calendar) calendar.clone();
	    // first day of this week
	    calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
	    // last day of previous week
	    calendar.add(Calendar.DAY_OF_MONTH, 7);
	    
	    calendar.set(Calendar.HOUR_OF_DAY, 23);
	    calendar.set(Calendar.MINUTE, 59);
	    calendar.set(Calendar.SECOND, 59);
	    calendar.set(Calendar.MILLISECOND, 0);
	    //add 4 hrs to solve hive timezone issue
	    //calendar.add(Calendar.HOUR_OF_DAY, 4);
	    endDate = dateFormatter.format(calendar.getTime()).toString();
	    //System.out.println("Week endDate is " + endDate);
	    dateList.add(startDate);
	    dateList.add(endDate);
	    return dateList;
		
	}
	
	public ArrayList<String> dateCalcuateForDailyReport(SimpleDateFormat dateFormatter){
		ArrayList<String> dateList = new ArrayList<String>();
	    String startDate = null;
	    String endDate = null;
	    
	    Calendar calendar = Calendar.getInstance();
	    calendar.add(Calendar.DAY_OF_YEAR, -1);
	    
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    //add 4 hrs to solve hive timezone issue
	    //calendar.add(Calendar.HOUR_OF_DAY, 4);
	    
	    startDate = dateFormatter.format(calendar.getTime()).toString();
	   // System.out.println("daily startDate is" + startDate);
	    
	    calendar = (Calendar) calendar.clone();
	    calendar.set(Calendar.HOUR_OF_DAY, 23);
	    calendar.set(Calendar.MINUTE, 59);
	    calendar.set(Calendar.SECOND, 59);
	    calendar.set(Calendar.MILLISECOND, 999);
	    //add 4 hrs to solve hive timezone issue
	    //calendar.add(Calendar.HOUR_OF_DAY, 4);
	    
	    endDate = dateFormatter.format(calendar.getTime()).toString();
	  //  System.out.println("daily endDate is " + endDate);
	    dateList.add(startDate);
	    dateList.add(endDate);
	    return dateList;
		
	}
}
