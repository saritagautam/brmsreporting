package com.lendinpoint.dao;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.lendinpoint.model.Config;

@Repository("configDao")
public class ConfigDaoImpl implements ConfigDao{
	@Autowired
	@Qualifier(value="sessionFactory")
	private SessionFactory sessionFactory;
	
	@Override
	public String getConfigByName(String configName) {
		Query query= sessionFactory.getCurrentSession().
		        createQuery("Select configValue from Config where configName=:configName");
		query.setParameter("configName", configName);
		String configValue = (String)query.uniqueResult();
		return configValue;
	}

	@Override
	public String getConfigById(Long id) {
		Config config = (Config)sessionFactory.getCurrentSession().get(Config.class, id);
		if(config != null)
			return config.getConfigValue();
		else
			return null;
	}
}
