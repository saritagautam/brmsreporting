package com.lendinpoint.dao;

public interface ConfigDao {
	public String getConfigByName(String configName);
	public String getConfigById(Long id);
}
