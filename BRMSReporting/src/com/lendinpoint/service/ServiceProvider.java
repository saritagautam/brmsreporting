package com.lendinpoint.service;

import org.springframework.beans.factory.annotation.Autowired;

public class ServiceProvider {
	@Autowired
	ConfigService configService;
	
	public String getConfigByName(String configName) {
		return configService.getConfigByName(configName);
	}
	
	public String getConfigById(Long id) {
		return configService.getConfigById(id);
	}
}
