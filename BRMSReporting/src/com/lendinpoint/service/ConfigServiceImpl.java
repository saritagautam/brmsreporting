package com.lendinpoint.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lendinpoint.dao.ConfigDao;

@Service("configService")
@Transactional
public class ConfigServiceImpl implements ConfigService{
	@Autowired 
	ConfigDao configDao;
	@Override
	public String getConfigByName(String configName) {
		return configDao.getConfigByName(configName);
	}
	@Override
	public String getConfigById(Long id) {
		return configDao.getConfigById(id);
	}
}
