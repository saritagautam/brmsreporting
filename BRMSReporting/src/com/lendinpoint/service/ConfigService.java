package com.lendinpoint.service;

public interface ConfigService {
	public String getConfigByName(String configName);
	public String getConfigById(Long id);
}
