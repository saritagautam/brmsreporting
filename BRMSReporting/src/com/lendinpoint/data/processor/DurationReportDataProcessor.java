package com.lendinpoint.data.processor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.lendinpoint.Queries.DurationReportQueries;
import com.lendinpoint.Queries.RulesViolatedQueries;
import com.lendinpoint.common.DateCalculator;
import com.lendinpoint.common.HiveConnector;
import com.lendinpoint.model.DurationReportData;
import com.lendinpoint.model.RulesViolatedData;

public class DurationReportDataProcessor {

	@Autowired
	HiveConnector hiveConnector;
	
	@Autowired
	DateCalculator dateCalculator;
	
	Connection hiveConnection = null;
	
	Logger logger = LoggerFactory.getLogger(DurationReportDataProcessor.class);
	
	public List<DurationReportData> durationReport (int days){
		try {
			if(hiveConnection == null || hiveConnection.isClosed())
				hiveConnection = hiveConnector.getHiveConnction();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e1.printStackTrace(new PrintWriter(sw));
	        logger.error("error in getting hive connection : "+ sw.toString());
			hiveConnector.getHiveConnction();
		}
		
		SimpleDateFormat creditReportIntervalDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		ArrayList<String> datesListForQuery = new ArrayList<String>();
		if(days==1)
			datesListForQuery =	dateCalculator.dateCalcuateForDailyReport(creditReportIntervalDateFormat);
		else if(days==7)
			datesListForQuery =	dateCalculator.dateCalcuateForWeeklyReport(creditReportIntervalDateFormat);
		else if(days>7)
			datesListForQuery =	dateCalculator.dateCalcuateForMonthlyReport(creditReportIntervalDateFormat);
		
		List<DurationReportData> dataList = new ArrayList<DurationReportData>();
		try {
			Calendar start = Calendar.getInstance();
			Calendar stop = Calendar.getInstance();
			Statement statement = hiveConnection.createStatement();
			ResultSet resultSet = statement
					.executeQuery(DurationReportQueries.DURATION_REPORT_QUERY.replace("[STARTDATE]",datesListForQuery.get(0)).replace("[ENDDATE]",datesListForQuery.get(1)));
			while (resultSet.next()) {
				DurationReportData durationData = new DurationReportData();
				durationData.setApplicationId(resultSet.getString("ApplicationID"));
				durationData.setCustomerId(resultSet.getString("CustomerID"));
				durationData.setLoanId(resultSet.getString("LoanID"));
				durationData.setSessionId(resultSet.getString("SessionID"));
				durationData.setStartTime(resultSet.getTimestamp("StartTime"));
				durationData.setStopTime(resultSet.getTimestamp("StopTime"));
				
				start.setTimeInMillis( resultSet.getTimestamp("StartTime").getTime() );
				
				stop.setTimeInMillis( resultSet.getTimestamp("StopTime").getTime() );
				int elapsedTime = (int) (stop.getTimeInMillis()-start.getTimeInMillis());
				durationData.setElapsedTime(elapsedTime);
				dataList.add(durationData);
            }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dataList;
	}
	
	public void closeConnection(){
		try {
			if(hiveConnection != null ){
				hiveConnection.close();
				logger.debug("Hive Connection closed");
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e1.printStackTrace(new PrintWriter(sw));
	        logger.error("error in closing hive connection : "+ sw.toString());
		}
	}
}
