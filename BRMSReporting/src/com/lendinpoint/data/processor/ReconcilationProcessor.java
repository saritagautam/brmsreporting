package com.lendinpoint.data.processor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.lendinpoint.ReportController;
import com.lendinpoint.Queries.ReconcilationQueries;
import com.lendinpoint.common.HiveConnector;
import com.lendinpoint.model.ReconcilationData;

public class ReconcilationProcessor {
	@Autowired
	HiveConnector hiveConnector;
	
	Connection hiveConnection = null;
	
	 /*Calendar cal = Calendar.getInstance();
     int days = cal.getActualMaximum(Calendar.DAY_OF_MONTH);*/
	Logger logger = LoggerFactory.getLogger(ReconcilationProcessor.class);
	
	public Long totalBRMSRequestCount(ArrayList<String> datesListForQuery){
		try {
			if(hiveConnection == null || hiveConnection.isClosed())
				hiveConnection = hiveConnector.getHiveConnction();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e1.printStackTrace(new PrintWriter(sw));
	        logger.error("error in getting hive connection : "+ sw.toString());
			hiveConnector.getHiveConnction();
		}
		Long count = (long) 0;
		try {
			Statement statement = hiveConnection.createStatement();
			System.out.println("before exceuting totalBRMSRequestCount");
			ResultSet resultSet = statement
					.executeQuery(ReconcilationQueries.TOTAL_NUMBER_OF_BRMS_REQUESTS.replace("[STARTDATE]",datesListForQuery.get(0)).replace("[ENDDATE]",datesListForQuery.get(1)));
			System.out.println("afer exceuting totalBRMSRequestCount");
			while (resultSet.next()) {
                System.out.println(resultSet.getLong(1));
                count = resultSet.getLong(1);
            }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("error in reconcilation : "+sw.toString());
		}
		return count;
	}
	
	public List<ReconcilationData> totalGDSRequest(ArrayList<String> datesListForQuery){
		try {
			if(hiveConnection == null || hiveConnection.isClosed())
				hiveConnection = hiveConnector.getHiveConnction();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e1.printStackTrace(new PrintWriter(sw));
	        logger.error("error in getting hive connection : "+ sw.toString());
			hiveConnector.getHiveConnction();
		}
		List<ReconcilationData> dataList = new ArrayList<ReconcilationData>();
		
		try {
			Statement statement = hiveConnection.createStatement();
			System.out.println("before exceuting totalGDSRequest");
			ResultSet resultSet = statement
					.executeQuery(ReconcilationQueries.TOTAL_BRMS_REQUESTS_DATA.replace("[STARTDATE]",datesListForQuery.get(0)).replace("[ENDDATE]",datesListForQuery.get(1)));
			System.out.println("after exceuting totalGDSRequest");
			while (resultSet.next()) {
				ReconcilationData reconcilationData = new ReconcilationData();
				reconcilationData.setApplicationId(resultSet.getString("ApplicationID"));
				reconcilationData.setCustomerId(resultSet.getString("CustomerID"));
				reconcilationData.setLoanId(resultSet.getString("LoanID"));
				reconcilationData.setSessionId(resultSet.getString("SessionID"));
				dataList.add(reconcilationData);
            }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("error in reconcilation : "+sw.toString());
		}
		return dataList;
	}
	
	public List<ReconcilationData> totalGDSRequestFirstTime(ArrayList<String> datesListForQuery){
		try {
			if(hiveConnection == null || hiveConnection.isClosed())
				hiveConnection = hiveConnector.getHiveConnction();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e1.printStackTrace(new PrintWriter(sw));
	        logger.error("error in getting hive connection : "+ sw.toString());
			hiveConnector.getHiveConnction();
		}
		List<ReconcilationData> dataList = new ArrayList<ReconcilationData>();
		
		try {
			Statement statement = hiveConnection.createStatement();
			System.out.println("before exceuting totalGDSRequestFirstTime");
			ResultSet resultSet = statement
					.executeQuery(ReconcilationQueries.GDS_REQUESTS_FIRST_TIME_IN_LAST_30_DAYS.replace("[STARTDATE]",datesListForQuery.get(0)).replace("[ENDDATE]",datesListForQuery.get(1)));
			System.out.println("after exceuting totalGDSRequestFirstTime");
			while (resultSet.next()) {
				ReconcilationData reconcilationData = new ReconcilationData();
				reconcilationData.setApplicationId(resultSet.getString("ApplicationID"));
				reconcilationData.setCustomerId(resultSet.getString("CustomerID"));
				reconcilationData.setLoanId(resultSet.getString("LoanID"));
				dataList.add(reconcilationData);
            }
		} catch (SQLException e) {
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("error in reconcilation : "+sw.toString());
		}
		return dataList;
	}
	
	public List<ReconcilationData> gdsRequestsMoreThanOnce(ArrayList<String> datesListForQuery){
		try {
			if(hiveConnection == null || hiveConnection.isClosed())
				hiveConnection = hiveConnector.getHiveConnction();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e1.printStackTrace(new PrintWriter(sw));
	        logger.error("error in getting hive connection : "+ sw.toString());
			hiveConnector.getHiveConnction();
		}
		List<ReconcilationData> dataList = new ArrayList<ReconcilationData>();
		
		try {
			Statement statement = hiveConnection.createStatement();
			logger.debug("before exceuting gdsRequestsMoreThanOnce");
			ResultSet resultSet = statement
					.executeQuery(ReconcilationQueries.GDS_REQUESTS_SENT_MORE_THAN_ONCE_IN_30_DAYS.replace("[STARTDATE]",datesListForQuery.get(0)).replace("[ENDDATE]",datesListForQuery.get(1)));
			logger.debug("after exceuting gdsRequestsMoreThanOnce");
			while (resultSet.next()) {
				ReconcilationData reconcilationData = new ReconcilationData();
				reconcilationData.setApplicationId(resultSet.getString("ApplicationID"));
				reconcilationData.setCustomerId(resultSet.getString("CustomerID"));
				reconcilationData.setLoanId(resultSet.getString("LoanID"));
				reconcilationData.setAttemptsCount(resultSet.getString("AttemptsCount"));
				dataList.add(reconcilationData);
            }
		} catch (SQLException e) {
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("error in reconcilation : "+sw.toString());
		}
		return dataList;
	}
	
	public List<ReconcilationData> gdsRequestsErroredOut(ArrayList<String> datesListForQuery){
		try {
			if(hiveConnection == null || hiveConnection.isClosed())
				hiveConnection = hiveConnector.getHiveConnction();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e1.printStackTrace(new PrintWriter(sw));
	        logger.error("error in getting hive connection : "+ sw.toString());
			hiveConnector.getHiveConnction();
		}
		List<ReconcilationData> dataList = new ArrayList<ReconcilationData>();
		
		try {
			Statement statement = hiveConnection.createStatement();
			logger.debug("before exceuting gdsRequestsErroredOut");
			ResultSet resultSet = statement
					.executeQuery(ReconcilationQueries.GDS_REQUESTS_WHICH_ERRORED_OUT_IN_30_DAYS.replace("[STARTDATE]",datesListForQuery.get(0)).replace("[ENDDATE]",datesListForQuery.get(1)));
			logger.debug("after exceuting gdsRequestsErroredOut");
			while (resultSet.next()) {
				ReconcilationData reconcilationData = new ReconcilationData();
				reconcilationData.setApplicationId(resultSet.getString("ApplicationID"));
				reconcilationData.setCustomerId(resultSet.getString("CustomerID"));
				reconcilationData.setLoanId(resultSet.getString("LoanID"));
				reconcilationData.setSessionId(resultSet.getString("SessionID"));
				dataList.add(reconcilationData);
            }
		} catch (SQLException e) {
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("error in reconcilation : "+sw.toString());
		}
		return dataList;
	}
	
	public List<ReconcilationData> gdsRequestsSentFirstTimeErroredOut(ArrayList<String> datesListForQuery){
		try {
			if(hiveConnection == null || hiveConnection.isClosed())
				hiveConnection = hiveConnector.getHiveConnction();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e1.printStackTrace(new PrintWriter(sw));
	        logger.error("error in getting hive connection : "+ sw.toString());
			hiveConnector.getHiveConnction();
		}
		List<ReconcilationData> dataList = new ArrayList<ReconcilationData>();
		
		try {
			Statement statement = hiveConnection.createStatement();
			logger.debug("before exceuting gdsRequestsSentFirstTimeErroredOut");
			ResultSet resultSet = statement
					.executeQuery(ReconcilationQueries.GDS_REQUESTS_SENT_FIRST_TIME_WHICH_ERRORED_OUT_IN_30_DAYS.replace("[STARTDATE]",datesListForQuery.get(0)).replace("[ENDDATE]",datesListForQuery.get(1)));
			logger.debug("after exceuting gdsRequestsSentFirstTimeErroredOut");
			while (resultSet.next()) {
				ReconcilationData reconcilationData = new ReconcilationData();
				reconcilationData.setApplicationId(resultSet.getString("ApplicationID"));
				reconcilationData.setCustomerId(resultSet.getString("CustomerID"));
				reconcilationData.setLoanId(resultSet.getString("LoanID"));
				reconcilationData.setSessionId(resultSet.getString("SessionID"));
				dataList.add(reconcilationData);
            }
		} catch (SQLException e) {
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("error in reconcilation : "+sw.toString());
		}
		return dataList;
	}
	
	public List<ReconcilationData> gdsRequestSentMoreThanOnceErroredOut(ArrayList<String> datesListForQuery){
		try {
			if(hiveConnection == null || hiveConnection.isClosed())
				hiveConnection = hiveConnector.getHiveConnction();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e1.printStackTrace(new PrintWriter(sw));
	        logger.error("error in getting hive connection : "+ sw.toString());
			hiveConnector.getHiveConnction();
		}
		List<ReconcilationData> dataList = new ArrayList<ReconcilationData>();
		
		try {
			Statement statement = hiveConnection.createStatement();
			logger.debug("before exceuting gdsRequestSentMoreThanOnceErroredOut");
			ResultSet resultSet = statement
					.executeQuery(ReconcilationQueries.GDS_REQUESTS_SENT_MORE_THAN_ONCE_ERRORED_OUT.replace("[STARTDATE]",datesListForQuery.get(0)).replace("[ENDDATE]",datesListForQuery.get(1)));
			logger.debug("after exceuting gdsRequestSentMoreThanOnceErroredOut");
			while (resultSet.next()) {
				ReconcilationData reconcilationData = new ReconcilationData();
				reconcilationData.setApplicationId(resultSet.getString("ApplicationID"));
				reconcilationData.setCustomerId(resultSet.getString("CustomerID"));
				reconcilationData.setLoanId(resultSet.getString("LoanID"));
				reconcilationData.setAttemptsCount(resultSet.getString("AttemptsCount"));
				dataList.add(reconcilationData);
            }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("error in reconcilation : "+sw.toString());
		}
		return dataList;
	}
	
	public void closeConnection(){
		try {
			if(hiveConnection != null ){
				hiveConnection.close();
				logger.debug("Hive Connection closed");
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e1.printStackTrace(new PrintWriter(sw));
	        logger.error("error in closing hive connection : "+ sw.toString());
		}
	}
}
