package com.lendinpoint.data.processor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.lendinpoint.Queries.RulesViolatedQueries;
import com.lendinpoint.common.DateCalculator;
import com.lendinpoint.common.HiveConnector;
import com.lendinpoint.model.RulesViolatedData;
import com.lendinpoint.model.RulesViolatedSummaryData;

public class RulesViolatedDataProcessor {

	@Autowired
	HiveConnector hiveConnector;
	
	@Autowired
	DateCalculator dateCalculator;
	
	Connection hiveConnection = null;
	
	Logger logger = LoggerFactory.getLogger(RulesViolatedDataProcessor.class);
	
	SimpleDateFormat creditReportIntervalDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	public List<RulesViolatedData> rulesViolated(int days){
		try {
			if(hiveConnection == null || hiveConnection.isClosed())
				hiveConnection = hiveConnector.getHiveConnction();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e1.printStackTrace(new PrintWriter(sw));
	        logger.error("error in getting hive connection : "+ sw.toString());
			hiveConnector.getHiveConnction();
		}
		List<RulesViolatedData> dataList = new ArrayList<RulesViolatedData>();
		
		ArrayList<String> datesListForQuery = new ArrayList<String>();
		if(days==1)
			datesListForQuery =	dateCalculator.dateCalcuateForDailyReport(creditReportIntervalDateFormat);
		else if(days==7)
			datesListForQuery =	dateCalculator.dateCalcuateForWeeklyReport(creditReportIntervalDateFormat);
		else if(days>7)
			datesListForQuery =	dateCalculator.dateCalcuateForMonthlyReport(creditReportIntervalDateFormat);
		
		try {
			Statement statement = hiveConnection.createStatement();
			ResultSet resultSet = statement
					.executeQuery(RulesViolatedQueries.RULES_VIOLATED.replace("[STARTDATE]",datesListForQuery.get(0)).replace("[ENDDATE]",datesListForQuery.get(1)));
			while (resultSet.next()) {
				RulesViolatedData rulesViolatedData = new RulesViolatedData();
				rulesViolatedData.setApplicationId(resultSet.getString("ApplicationID"));
				rulesViolatedData.setCustomerId(resultSet.getString("CustomerID"));
				rulesViolatedData.setLoanId(resultSet.getString("LoanID"));
				rulesViolatedData.setSessionId(resultSet.getString("SessionID"));
				rulesViolatedData.setStatus(resultSet.getString("Status"));
				rulesViolatedData.setAppFundedDate(resultSet.getString("appFundedDate"));
				rulesViolatedData.setCreatedDate(resultSet.getString("createdDate"));
				rulesViolatedData.setGrade(resultSet.getString("grade"));
				rulesViolatedData.setModelName(resultSet.getString("modelName"));
				rulesViolatedData.setRuleName(resultSet.getString("ruleName"));
				rulesViolatedData.setStatusDate(resultSet.getString("statusDate"));
				dataList.add(rulesViolatedData);
            }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dataList;
	}
	
	public LinkedHashMap<String, RulesViolatedSummaryData> rulesViolatedSummary(int days) {
		try {
			if(hiveConnection == null || hiveConnection.isClosed())
				hiveConnection = hiveConnector.getHiveConnction();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e1.printStackTrace(new PrintWriter(sw));
	        logger.error("error in getting hive connection : "+ sw.toString());
			hiveConnector.getHiveConnction();
		}
		
		ArrayList<String> datesListForQuery = new ArrayList<String>();
		if(days==1)
			datesListForQuery =	dateCalculator.dateCalcuateForDailyReport(creditReportIntervalDateFormat);
		else if(days==7)
			datesListForQuery =	dateCalculator.dateCalcuateForWeeklyReport(creditReportIntervalDateFormat);
		else if(days>7)
			datesListForQuery =	dateCalculator.dateCalcuateForMonthlyReport(creditReportIntervalDateFormat);
		
		
		List<RulesViolatedSummaryData> dataList = new ArrayList<RulesViolatedSummaryData>();
		LinkedHashMap<String, RulesViolatedSummaryData> rulesDataMap = new LinkedHashMap<String, RulesViolatedSummaryData>();
		try {
			RulesViolatedSummaryData data = new RulesViolatedSummaryData();
			Statement statement = hiveConnection.createStatement();
			ResultSet resultSet = statement
					.executeQuery(RulesViolatedQueries.RULES_VIOLATED_SUMMARY.replace("[STARTDATE]",datesListForQuery.get(0)).replace("[ENDDATE]",datesListForQuery.get(1)));
			while (resultSet.next()) {
				
				RulesViolatedSummaryData rulesViolatedData = new RulesViolatedSummaryData();
				rulesViolatedData.setRuleName(resultSet.getString("ruleName"));
				
				if(resultSet.getString("ruleStatus").equalsIgnoreCase("Pass"))
					rulesViolatedData.setRulePassCount(resultSet.getInt("ruleCount"));
				else if(resultSet.getString("ruleStatus").equalsIgnoreCase("Fail"))
					rulesViolatedData.setRuleFailCount(resultSet.getInt("ruleCount"));
				if (rulesDataMap.containsKey(resultSet.getString("ruleName"))) {
					data = rulesDataMap.get(resultSet.getString("ruleName"));
					if(rulesViolatedData.getRuleFailCount() == null || rulesViolatedData.getRuleFailCount() ==0)
						rulesViolatedData.setRuleFailCount(data.getRuleFailCount());
					if(rulesViolatedData.getRulePassCount() == null || rulesViolatedData.getRulePassCount() == 0)
						rulesViolatedData.setRulePassCount(data.getRulePassCount());
					
				}
				if(rulesViolatedData.getRuleFailCount() == null)
					rulesViolatedData.setRuleFailCount(0);
				if(rulesViolatedData.getRulePassCount() == null)
					rulesViolatedData.setRulePassCount(0);
				rulesDataMap.put(resultSet.getString("ruleName"),rulesViolatedData);
												
            }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rulesDataMap;
	}
	
	public void closeConnection(){
		try {
			if(hiveConnection != null ){
				hiveConnection.close();
				logger.debug("Hive Connection closed");
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e1.printStackTrace(new PrintWriter(sw));
	        logger.error("error in closing hive connection : "+ sw.toString());
		}
	}
}
