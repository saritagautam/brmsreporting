package com.lendinpoint.data.processor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.lendinpoint.Queries.ErrorQueries;
import com.lendinpoint.Queries.ReconcilationQueries;
import com.lendinpoint.common.DateCalculator;
import com.lendinpoint.common.HiveConnector;
import com.lendinpoint.model.ErrorReportData;
import com.lendinpoint.model.ErrorSummaryData;
import com.lendinpoint.model.ReconcilationData;
import com.lendinpoint.report.processor.ErrorReportProcessor;

public class ErrorDataProcessor {
	
	@Autowired
	HiveConnector hiveConnector;
	
	@Autowired
	ErrorReportProcessor errorReportProcessor;
	
	@Autowired
	DateCalculator dateCalculator;
	
	Connection hiveConnection = null;
	
	SimpleDateFormat errorDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
	Logger logger = LoggerFactory.getLogger(ErrorDataProcessor.class);
	
	public List<ErrorReportData> errorReport(int days){
		try {
			if(hiveConnection == null || hiveConnection.isClosed())
				hiveConnection = hiveConnector.getHiveConnction();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e1.printStackTrace(new PrintWriter(sw));
	        logger.error("error in getting hive connection : "+ sw.toString());
			hiveConnector.getHiveConnction();
		}
		List<ErrorReportData> dataList = new ArrayList<ErrorReportData>();
		SimpleDateFormat creditReportIntervalDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		ArrayList<String> datesListForQuery = new ArrayList<String>();
		if(days==1)
			datesListForQuery =	dateCalculator.dateCalcuateForDailyReport(creditReportIntervalDateFormat);
		else if(days==7)
			datesListForQuery =	dateCalculator.dateCalcuateForWeeklyReport(creditReportIntervalDateFormat);
		else if(days>7)
			datesListForQuery =	dateCalculator.dateCalcuateForMonthlyReport(creditReportIntervalDateFormat);
		try {
			Statement statement = hiveConnection.createStatement();
			ResultSet resultSet = statement
					.executeQuery(ErrorQueries.ERROR_REPORT_QUERY.replace("[STARTDATE]",datesListForQuery.get(0)).replace("[ENDDATE]",datesListForQuery.get(1)));
			while (resultSet.next()) {
				ErrorReportData errorData = new ErrorReportData();
				errorData.setApplicationId(resultSet.getString("ApplicationID"));
				errorData.setCustomerId(resultSet.getString("CustomerID"));
				errorData.setLoanId(resultSet.getString("LoanID"));
				errorData.setSessionId(resultSet.getString("SessionID"));
				errorData.setFsAssigned(resultSet.getString("FS_Assigned_To_Val__c"));
				errorData.setCsAssigned(resultSet.getString("CS_Assigned__c"));
				errorData.setContractStatus(resultSet.getString("ContractStatus"));
				try {
					if(resultSet.getString("ErrorDate") != null && resultSet.getString("ErrorDate") != ""){
						String reformattedStr = errorDateFormat.format(creditReportIntervalDateFormat.parse(resultSet.getString("ErrorDate")));
				    	errorData.setErrorDate(reformattedStr);
				    }
				} catch (ParseException e) {
					StringWriter sw = new StringWriter();
			        e.printStackTrace(new PrintWriter(sw));
					logger.error("Exception in parsing in error data processor "+sw.toString());
				}
				
				errorData.setError(resultSet.getString("Error"));
				dataList.add(errorData);
            }
		} catch (SQLException e) {
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("Exception in error data processor "+sw.toString());
		}
		return dataList;
	}
	
	public List<ErrorSummaryData> summaryData(int days){
		try {
			if(hiveConnection == null || hiveConnection.isClosed())
				hiveConnection = hiveConnector.getHiveConnction();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e1.printStackTrace(new PrintWriter(sw));
	        logger.error("error in getting hive connection : "+ sw.toString());
			hiveConnector.getHiveConnction();
		}
		
		SimpleDateFormat creditReportIntervalDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		ArrayList<String> datesListForQuery = new ArrayList<String>();
		if(days==1)
			datesListForQuery =	dateCalculator.dateCalcuateForDailyReport(creditReportIntervalDateFormat);
		else if(days==7)
			datesListForQuery =	dateCalculator.dateCalcuateForWeeklyReport(creditReportIntervalDateFormat);
		else if(days>7)
			datesListForQuery =	dateCalculator.dateCalcuateForMonthlyReport(creditReportIntervalDateFormat);
		
		List<ErrorSummaryData> dataList = new ArrayList<ErrorSummaryData>();
		try {
			Statement statement = hiveConnection.createStatement();
			ResultSet resultSet = statement
					.executeQuery(ErrorQueries.ERRORS_SUMMARY.replace("[STARTDATE]",datesListForQuery.get(0)).replace("[ENDDATE]",datesListForQuery.get(1)));
			while (resultSet.next()) {
				ErrorSummaryData errorData = new ErrorSummaryData();
				errorData.setCount(resultSet.getInt("countTotal"));
				errorData.setError(resultSet.getString("errormessage"));
				dataList.add(errorData);
            }
		} catch (SQLException e) {
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
	        logger.error("Exception in error data processor "+sw.toString());
		}
		return dataList;
	}
	
	public int totalRequests(int days){
		try {
			if(hiveConnection == null || hiveConnection.isClosed())
				hiveConnection = hiveConnector.getHiveConnction();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e1.printStackTrace(new PrintWriter(sw));
	        logger.error("error in getting hive connection : "+ sw.toString());
			hiveConnector.getHiveConnction();
		}
		
		SimpleDateFormat creditReportIntervalDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		ArrayList<String> datesListForQuery = new ArrayList<String>();
		if(days==1)
			datesListForQuery =	dateCalculator.dateCalcuateForDailyReport(creditReportIntervalDateFormat);
		else if(days==7)
			datesListForQuery =	dateCalculator.dateCalcuateForWeeklyReport(creditReportIntervalDateFormat);
		else if(days>7)
			datesListForQuery =	dateCalculator.dateCalcuateForMonthlyReport(creditReportIntervalDateFormat);
		
		int totalCount=0;
		try {
			Statement statement = hiveConnection.createStatement();
			ResultSet resultSet = statement
					.executeQuery(ErrorQueries.TOTAL_NUMBER_OF_REQUESTS.replace("[STARTDATE]",datesListForQuery.get(0)).replace("[ENDDATE]",datesListForQuery.get(1)));
			while (resultSet.next()) {
                totalCount = (int) resultSet.getLong(1);
            }
            
		} catch (SQLException e) {
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
	        logger.error("Exception in error data processor "+sw.toString());
		}
		return totalCount;
	}
	
	public void closeConnection(){
		try {
			if(hiveConnection != null ){
				hiveConnection.close();
				logger.debug("Hive Connection closed");
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
	        e1.printStackTrace(new PrintWriter(sw));
	        logger.error("error in closing hive connection : "+ sw.toString());
		}
	}
}
