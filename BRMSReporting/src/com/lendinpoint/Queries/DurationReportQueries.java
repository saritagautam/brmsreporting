package com.lendinpoint.Queries;

public class DurationReportQueries {
	public static final String DURATION_REPORT_QUERY = ""
			+ "SELECT grd.applicationid ApplicationID, c.customer_number__c CustomerID, "
			+ "llac.name LoanID,grd.sessionid SessionID, grd.RULESEXECUTIONSTARTDATETIME StartTime,"
			+ "grd.RULESEXECUTIONSTOPDATETIME StopTime FROM brmsreporting.brms_gds_response grd "
			+ "LEFT JOIN brmsreporting.genesis__Applications__c app on grd.applicationid = app.id "
			+ "LEFT JOIN brmsreporting.contact c on c.ID=app.genesis__Contact__c "
			+ "LEFT JOIN brmsreporting.loan__loan_account__c llac on llac.application__c = app.id "
			+ "Where grd.datecreated >='[STARTDATE]' and grd.datecreated <='[ENDDATE]'"
			+ "AND grd.RULESEXECUTIONSTARTDATETIME is not NULL";
}