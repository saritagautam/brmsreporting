package com.lendinpoint.Queries;

public class ReconcilationQueries {
	public static final String TOTAL_NUMBER_OF_BRMS_REQUESTS = ""
			+ "SELECT count(*) FROM brmsreporting.brms_gds_request grd "
			+ "Where grd.datecreated >='[STARTDATE]' and grd.datecreated <='[ENDDATE]' ";
	
	public static final String TOTAL_BRMS_REQUESTS_DATA = ""
			+ "SELECT grd.applicationid ApplicationID, c.customer_number__c CustomerID, "
			+ "llac.name LoanID,grd.sessionid SessionID FROM brmsreporting.brms_gds_request grd "
			+ "LEFT JOIN brmsreporting.genesis__Applications__c app on grd.applicationid = app.id "
			+ "LEFT JOIN brmsreporting.contact c on c.ID=app.genesis__Contact__c "
			+ "LEFT JOIN brmsreporting.loan__loan_account__c llac on llac.application__c = app.id "
			+ "Where grd.datecreated >='[STARTDATE]' and grd.datecreated <='[ENDDATE]' ";
	
	 public static final String GDS_REQUESTS_FIRST_TIME_IN_LAST_30_DAYS = ""
			+ "select app.id ApplicationID ,c.customer_number__c CustomerID, llac.name LoanID, app.createdDate "
			+ "from brmsreporting.genesis__Applications__c app "
			+ "LEFT JOIN brmsreporting.brms_gds_request grd on app.id = grd.applicationid "
			+ "LEFT JOIN brmsreporting.contact c on c.ID=app.genesis__Contact__c "
			+ "LEFT JOIN brmsreporting.loan__loan_account__c llac on llac.application__c = app.id "
			+ "where  app.createddate >='[STARTDATE]' and app.createddate <='[ENDDATE]' "
			+ "and grd.sessionid is not null "
			+ "group by app.id,app.createdDate,c.customer_number__c,llac.name ";
	 
	 public static final String GDS_REQUESTS_SENT_MORE_THAN_ONCE_IN_30_DAYS ="SELECT grd.applicationid ApplicationID,"
	 		+ "c.customer_number__c CustomerID, la.name LoanID, "
	 		+ "count (grd.applicationid) as AttemptsCount FROM brmsreporting.brms_gds_request grd "
	 		+ "LEFT JOIN brmsreporting.genesis__Applications__c app on grd.applicationid = app.id "
	 		+ "LEFT JOIN brmsreporting.loan__loan_account__c llac on llac.application__c = app.id "
	 		+ "LEFT JOIN brmsreporting.contact c on c.ID=app.genesis__Contact__c "
	 		+ "LEFT JOIN brmsreporting.loan__loan_account__c la on la.application__c = app.id "
	 		+ "where grd.datecreated >='[STARTDATE]' and grd.datecreated <='[ENDDATE]' "
	 		+ "GROUP BY grd.applicationid,c.customer_number__c,la.name "
	 		+ "Having count (grd.applicationid)>1";
	 
	 public static final String GDS_REQUESTS_WHICH_ERRORED_OUT_IN_30_DAYS =""
	 		+ "select distinct app.id ApplicationID,c.customer_number__c CustomerID,"
	 		+ "llac.name LoanID,grd.sessionid SessionID "
	 		+ "from brmsreporting.genesis__Applications__c app "
	 		+ "LEFT JOIN brmsreporting.contact c on c.ID=app.genesis__Contact__c "
	 		+ "Left join brmsreporting.brms_gds_request grd on grd.applicationid = app.id "
	 		+ "Left join brmsreporting.loan__loan_account__c llac on llac.application__c = app.id "
	 		+ "where app.id in ( select distinct applicationid from brmsreporting.brms_gds_error "
	 		+ "where datecreated >='[STARTDATE]' and datecreated <='[ENDDATE]') ";
	 
	 public static final String GDS_REQUESTS_SENT_FIRST_TIME_WHICH_ERRORED_OUT_IN_30_DAYS =""
	 		+ "select app.id ApplicationID,c.customer_number__c CustomerID,"
	 		+ "llac.name LoanID,ge.sessionid SessionID "
	 		+ "from brmsreporting.genesis__Applications__c app "
	 		+ "LEFT JOIN brmsreporting.contact c on c.ID=app.genesis__Contact__c "
	 		+ "LEFT JOIN brmsreporting.loan__loan_account__c llac on llac.application__c = app.id "
	 		+ "LEFT JOIN brmsreporting.brms_gds_error ge on app.id = ge.applicationid "
	 		+ "where app.createddate >='[STARTDATE]' and app.createddate <='[ENDDATE]' "
	 		+ "and ge.sessionid is not null";
	 
	 public static final String GDS_REQUESTS_SENT_MORE_THAN_ONCE_ERRORED_OUT =""
	 		+ "select app.id ApplicationID,c.customer_number__c CustomerID,llac.name LoanID,"
	 		+ "count (app.id) as AttemptsCount from brmsreporting.genesis__Applications__c app "
	 		+ "LEFT JOIN brmsreporting.contact c on c.ID=app.genesis__Contact__c "
	 		+ "Left join brmsreporting.brms_gds_request grd on grd.applicationid = app.id "
	 		+ "Left join brmsreporting.loan__loan_account__c llac on llac.application__c = app.id "
	 		+ "where app.id in ( select applicationid from brmsreporting.brms_gds_error gdse "
	 		+ "where gdse.datecreated >='[STARTDATE]' and gdse.datecreated <='[ENDDATE]' "
	 		+ "group by gdse.applicationid "
	 		+ "having count (gdse.applicationid)>1 ) "
	 		+ "group by app.id,c.customer_number__c,llac.name";
}
