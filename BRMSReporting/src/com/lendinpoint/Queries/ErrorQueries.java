package com.lendinpoint.Queries;

public class ErrorQueries {
	public static final String ERROR_REPORT_QUERY = ""
			+ "Select err.errormessage Error, err.dateCreated ErrorDate,err.applicationid ApplicationID, "
			+ "c.customer_number__c CustomerID, llac.name LoanID,err.sessionid SessionID,"
			+ "llac.loan__Loan_Status__c ContractStatus,"
			+ "app.FS_Assigned_To_Val__c,app.CS_Assigned__c "
			//+ "u.Name Owner "
			+ "FROM brmsreporting.brms_gds_error err "
			+ "Left join brmsreporting.genesis__Applications__c app on err.applicationid = app.id "
			+ "Left join brmsreporting.contact c on c.ID=app.genesis__Contact__c "
			+ "Left join brmsreporting.loan__loan_account__c llac on llac.application__c= app.id "
			//+ "Left join salesforce.`user` u on u.id = app.OwnerId "
			+ "Where err.Datecreated >='[STARTDATE]' and err.Datecreated <='[ENDDATE]' "
			+ "order by ErrorDate desc";
	
	public static final String ERROR_REPORT_QUERY_WEEKLY = ""
			+ "Select err.errormessage Error, err.dateCreated ErrorDate,err.applicationid ApplicationID, "
			+ "c.customer_number__c CustomerID, llac.name LoanID,err.sessionid SessionID,"
			+ "llac.loan__Loan_Status__c ContractStatus,"
			+ "app.FS_Assigned_To_Val__c,app.CS_Assigned__c "
			//+ "u.Name Owner "
			+ "FROM brmsreporting.brms_gds_error err "
			+ "Left join brmsreporting.genesis__Applications__c app on err.applicationid = app.id "
			+ "Left join brmsreporting.contact c on c.ID=app.genesis__Contact__c "
			+ "Left join brmsreporting.loan__loan_account__c llac on llac.application__c= app.id "
			//+ "Left join salesforce.`user` u on u.id = app.FS_Assigned_To_Val__c "
			+ "Where err.Datecreated >='[STARTDATE]' and err.Datecreated <='[ENDDATE]' "
			+ "order by ErrorDate desc";
			
	public static final String ERRORS_SUMMARY = ""
			+ "select errormessage ,count(errormessage) as countTotal FROM brmsreporting.brms_gds_error "
			+ "Where Datecreated >='[STARTDATE]' and Datecreated <='[ENDDATE]' "
			+ "group by errormessage";
	
	public static final String TOTAL_NUMBER_OF_REQUESTS = ""
			+ "SELECT count(*) FROM brmsreporting.brms_gds_response grd "
			+ "Where grd.Datecreated >='[STARTDATE]' and grd.Datecreated <='[ENDDATE]' ";
}
