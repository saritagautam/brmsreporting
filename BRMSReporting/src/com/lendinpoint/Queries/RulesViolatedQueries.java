package com.lendinpoint.Queries;

public class RulesViolatedQueries {

	public static final String RULES_VIOLATED = ""
			+ "select distinct rule.pk,rule.applicationid ApplicationID,rule.derulename ruleName,"
			+ "c.customer_number__c CustomerID, llac.name LoanID,rule.sessionid SessionID,"
			+ "app.createddate createdDate,gm.modelname modelName, gm.grade grade, "
			+ "app.genesis__status__c status,gah.createddate statusDate,app.funded_date__c appFundedDate "
			+ "from brmsreporting.brms_rules_result rule "
			+ "left join brmsreporting.genesis__Applications__c app on rule.applicationid = app.id "
			+ "Left join brmsreporting.contact c on c.ID=app.genesis__Contact__c "
			+ "LEFT JOIN brmsreporting.loan__loan_account__c llac on llac.application__c= app.id "
			+ "LEFT JOIN brmsreporting.gdsmodels gm on gm.sessionid= rule.sessionid "
			+ "LEFT JOIN brmsreporting.genesis__applications__history gah on gah.parentid = app.id "
			+ "Where rule.datecreated >='[STARTDATE]' and rule.datecreated <='[ENDDATE]' "
			+ "and rule.derulestatus = 'Fail' ";
	
	public static final String RULES_VIOLATED_SUMMARY = ""
			+ "select rule.derulename ruleName,rule.derulestatus ruleStatus, "
			+ "count(rule.derulename) as ruleCount,rule.rulepriority "
			+ "from brmsreporting.brms_rules_result rule "
			+ "Where rule.datecreated >='[STARTDATE]' and rule.datecreated <='[ENDDATE]' "
			+ "and rule.derulename is not null "
			+ "group by rule.derulename,rule.derulestatus,rule.rulepriority "
			+ "order by rule.rulepriority desc";
}
