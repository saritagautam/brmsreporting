package com.lendinpoint;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lendinpoint.common.DateCalculator;
import com.lendinpoint.common.ReportMailSender;
import com.lendinpoint.job.processor.DurationReportJobProcessor;
import com.lendinpoint.job.processor.ErrorReportJobProcessor;
import com.lendinpoint.job.processor.ReconcilationJobProcessor;
import com.lendinpoint.job.processor.RulesViolatedJobProcessor;
import com.lendinpoint.service.ServiceProvider;

@Controller
@RequestMapping("/brmsreports")
public class ReportController {

	@Autowired
	ReconcilationJobProcessor reconcilationJobProcessor;
	
	@Autowired
	ErrorReportJobProcessor errorReportJobProcessor;
	
	@Autowired
	RulesViolatedJobProcessor rulesViolatedJobProcessor;
	
	@Autowired
	DurationReportJobProcessor durationReportJobProcessor;
	
	@Autowired
	ServiceProvider serviceProvider;
	
	@Autowired
	ReportMailSender reportMailSender;
	
	@Autowired
	DateCalculator dateCalculator;
	
	Logger logger = LoggerFactory.getLogger(ReportController.class);
	
	@RequestMapping(value = "/generateReports", method = RequestMethod.GET)
	@ResponseBody
	private String generateReports(){
		String status="";
		String reconcilationFile = null;
		String errorReportFile = null;
		String rulesViolatedReportFile = null;
		String durationReportFile = null;
		String mailTo = serviceProvider.getConfigByName("mailTo");
		String mailBcc = serviceProvider.getConfigByName("mailBcc");
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat creditReportIntervalDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		if(cal.get(Calendar.DAY_OF_MONTH) == 1)//generate reconcilation report monthly
		{
			logger.debug("**************** Running monthly Reconcilation report ******************");
			try{
				dateCalculator.dateCalcuateForWeeklyReport(creditReportIntervalDateFormat);
				ArrayList<String> datesListForQuery=dateCalculator.dateCalcuateForMonthlyReport(creditReportIntervalDateFormat);
				cal.add(Calendar.MONTH, -1);
				cal.set(Calendar.DATE, 1);
				int days = cal.getActualMaximum(Calendar.DATE);
				reconcilationFile = reconcilationJobProcessor.generateReconcileReport(datesListForQuery);
			}catch(Exception e){
				logger.error("Exception in reconcilation report");
				e.printStackTrace();
			}
		}
		logger.debug("reconcilationFile : " + reconcilationFile);
		
		logger.debug("**************** Running Error report ******************");
		try{
			errorReportFile = errorReportJobProcessor.generateErrorReport();
		}catch(Exception e){
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("Exception in error report "+sw.toString());
			e.printStackTrace();
		}
		logger.debug("errorReportFile : "+errorReportFile);
		
		logger.debug("**************** Running Rules report ******************");
		try{
			rulesViolatedReportFile = rulesViolatedJobProcessor.generateRulesViolatedReport();
			logger.debug("rulesViolatedReportFile : "+rulesViolatedReportFile);
		}catch(Exception e){
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("Exception in rules violated report "+sw.toString());
			
		}
		
		logger.debug("**************** Running Duration report ******************");
		try{
			durationReportFile = durationReportJobProcessor.generateDurationReport();
			logger.debug("durationReportFile : "+durationReportFile);
		}catch(Exception e){
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
			logger.error("Exception in duration report "+sw.toString());
			e.printStackTrace();
		}
		
		if(reconcilationFile != null){
			reportMailSender.sendEmail(reconcilationFile, mailTo, mailBcc, "reconcile");
		}
		if(durationReportFile != null){
			reportMailSender.sendEmail(durationReportFile, mailTo, mailBcc, "duration");
		}
		if(errorReportFile != null){
			reportMailSender.sendEmail(errorReportFile, mailTo, mailBcc, "error");
		}
		if(rulesViolatedReportFile != null){
			reportMailSender.sendEmail(rulesViolatedReportFile, mailTo, mailBcc, "rules");
		}
		status = "Generate Report Process Complete";
		return status;
	}
	
	private ArrayList dateCalcuate(){
		ArrayList<String> dateList = new ArrayList();
	    String startDate = null;
	    String endDate = null;
	    String endDisplayDate = null;
	    SimpleDateFormat creditReportIntervalDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	    Calendar calendar = Calendar.getInstance();
	    calendar.add(Calendar.MONTH, -1);
	    calendar.set(Calendar.DATE, 1);
	    
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    
	    startDate = creditReportIntervalDateFormat.format(calendar.getTime()).toString();
	   logger.debug("startDate is" + startDate);
	    int ReportInterval = calendar.getActualMaximum(Calendar.DATE);
	   
	   logger.debug("ReportInterval is: "+ReportInterval);
	    calendar.add(Calendar.DATE, ReportInterval-1);
	   
	    calendar.set(Calendar.HOUR_OF_DAY, 23);
	    calendar.set(Calendar.MINUTE, 59);
	    calendar.set(Calendar.SECOND, 59);
	    calendar.set(Calendar.MILLISECOND, 999);
	    
	    endDate = creditReportIntervalDateFormat.format(calendar.getTime()).toString();
	   logger.debug("endDate is " + endDate);
	    dateList.add(startDate);
	    dateList.add(endDate);
	   logger.debug("returning dateList:"+dateList.isEmpty());
	    return dateList;
		
	}
}
