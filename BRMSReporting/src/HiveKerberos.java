import org.apache.hadoop.security.UserGroupInformation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class HiveKerberos {
private static final String CONF_KERBEROS_KEY = "hadoop.security.authentication";
private static final String CONF_KERBEROS_VALUE = "Kerberos";
private static final String HIVE_USER_PRINCIPAL = "hive/ip-172-42-90-11.lendingpoint.ad@LPQA.HDP";
private static final String HIVE_USER_KEYTAB = "C:\\Users\\sarita.gautam\\Desktop\\LendingPoint\\Hive\\conf_qa\\hive.qa.keytab";
private static final String HIVE_CONNECTION_STRING = "jdbc:hive2://34.196.201.199:10000/;principal=hive/ip-172-42-90-11.lendingpoint.ad@LPQA.HDP";
private static final String HIVE_JDBC_DRIVER = "org.apache.hive.jdbc.HiveDriver";
private static final String HIVE_QUERY = "SELECT grd.applicationid ApplicationID, c.customer_number__c CustomerID, llac.name LoanID,"
		+ "grd.sessionid SessionID"
		+ " FROM brms_reports.gdsrequestdata grd"
		+" LEFT JOIN salesforce.genesis__Applications__c app on grd.applicationid = app.id"
		+ " LEFT JOIN salesforce.contact c on c.ID=app.genesis__Contact__c "
		+ "LEFT JOIN salesforce.loan__loan_account__c llac on llac.application__c = app.id ";
		//+ "Where grd.datecreated >  DATE_SUB(FROM_UNIXTIME( UNIX_TIMESTAMP() ), 30);";

	public static void main(String args[]) {
	    try {
	        org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
	        conf.set(CONF_KERBEROS_KEY, CONF_KERBEROS_VALUE);
	        UserGroupInformation.setConfiguration(conf);
	        UserGroupInformation.loginUserFromKeytab(HIVE_USER_PRINCIPAL, HIVE_USER_KEYTAB);
	        Class.forName(HIVE_JDBC_DRIVER);
	        System.out.println("getting hiveConnection");
	        try (Connection hiveConnection = DriverManager.getConnection(HIVE_CONNECTION_STRING);
	             Statement statement = hiveConnection.createStatement();
	             ResultSet resultSet = statement.executeQuery(HIVE_QUERY);) {
	            while (resultSet.next()) {
	                System.out.println(resultSet.getString(1));
	            }
	        } catch (Exception exception) {
	            exception.printStackTrace();
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
}


